package stocktrainer.com.account.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import stocktrainer.com.R;
import stocktrainer.com.Utility.ConnectionDetector;
import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.account.server.AccountGETRequest;

public class MyAccountFragment extends Fragment implements OnChartValueSelectedListener {
    private Context mContext;
    View rootView;
    private ConnectionDetector cd;
    private PieChart mChart;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_account, container, false);
        mContext = getActivity();

        initComponent();

        return rootView;
    }


    private void initComponent(){
        cd =new ConnectionDetector(mContext);

        //set font style
        setFontStyle();

        if(cd.isConnectingToInternet()){
            getUserAccountDetails();
        }else{
            Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        if(cd.isConnectingToInternet()){
            getUserAccountDetails();
        }else{
            Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
        }

    }

    public void getUserAccountDetails() {
        new AccountGETRequest(mContext,new AccountGETRequest.AccountGETRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if (status.getCode() == 200) {
                            if(obj.getString("status").equals("true")){
                                setData(obj);
                            }else{
                                Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                            }
                        }else {
                            Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }

    private void setData(JSONObject jObject){
        try{
            JSONObject jObj=jObject.getJSONObject("data");
            System.out.println("jObj for account in SetData: "+jObj);
            PreferenceClass.setStringPreference(mContext,"ChartObject",jObj.toString());
            ((TextView)rootView.findViewById(R.id.txtSetStartingInvestment)).setText(mContext.getString(R.string.Rs)+jObj.getString("invest_limit"));
            ((TextView)rootView.findViewById(R.id.txtSetAvailableCash)).setText(mContext.getString(R.string.Rs)+jObj.getString("available_cash"));
            ((TextView)rootView.findViewById(R.id.txtSetStocksValue)).setText(mContext.getString(R.string.Rs)+jObj.getString("stock_value"));
            ((TextView)rootView.findViewById(R.id.txtSetTotalValue)).setText(mContext.getString(R.string.Rs)+jObj.getString("total_value"));
            ((TextView)rootView.findViewById(R.id.txtSetPosition)).setText(jObj.getString("position"));


            ((TextView)rootView.findViewById(R.id.txtSetTotalTransactoins)).setText(jObj.getJSONObject("transaction").getString("total"));
            ((TextView)rootView.findViewById(R.id.txtSetPositiveTransactoins)).setText(jObj.getJSONObject("transaction").getString("positive"));
            ((TextView)rootView.findViewById(R.id.txtSetNegativeTransactoins)).setText(jObj.getJSONObject("transaction").getString("negative"));
            setPieChartData();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setFontStyle(){

        ((TextView)rootView.findViewById(R.id.txtStartingInvestment)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtStocksValue)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtAvailableCash)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtTotalValue)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtPosition)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtTotalTransactoins)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtPositiveTransactoins)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtNegativeTransactoins)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

        ((TextView)rootView.findViewById(R.id.txtSetStartingInvestment)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)rootView.findViewById(R.id.txtSetStocksValue)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)rootView.findViewById(R.id.txtSetAvailableCash)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)rootView.findViewById(R.id.txtSetTotalValue)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)rootView.findViewById(R.id.txtSetPosition)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)rootView.findViewById(R.id.txtSetTotalTransactoins)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)rootView.findViewById(R.id.txtSetPositiveTransactoins)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)rootView.findViewById(R.id.txtSetNegativeTransactoins)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));

    }

    private void setPieChartData(){
        mChart = rootView.findViewById(R.id.pieChartAccount);

        mChart.setUsePercentValues(true);

        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
        ArrayList<Entry> yvalues = new ArrayList<>();
        ArrayList<String> xVals = new ArrayList<>();
        try {
            JSONObject jsonObjectChart = new JSONObject(PreferenceClass.getStringPreferences(mContext,"ChartObject"));
            JSONArray jArrayChart = jsonObjectChart.getJSONArray("graph_details");

            if(jArrayChart.length()>0 && jArrayChart!=null){
                for(int i = 0; i < jArrayChart.length(); i++) {
                    JSONObject object= jArrayChart.getJSONObject(i);
                    yvalues.add(new Entry(Float.parseFloat(object.getString("percent")), i));
                    xVals.add(""+object.getString("name"));
                    //entries1.add(new PieEntry((float) Float.parseFloat(object.getString("percent")), ));
                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        PieDataSet dataSet = new PieDataSet(yvalues, "Stocks Charts");
        PieData data = new PieData(xVals, dataSet);
        // In Percentage term
        data.setValueFormatter(new PercentFormatter());
        // Default value
        //data.setValueFormatter(new DefaultValueFormatter(0));
        mChart.setData(data);
        mChart.setDescription("");

        mChart.setDrawHoleEnabled(true);
        mChart.setTransparentCircleRadius(25f);
        mChart.setHoleRadius(25f);

        dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.DKGRAY);
        mChart.setOnChartValueSelectedListener(this);
        mChart.getLegend().setEnabled(false);
        mChart.animateXY(1400, 1400);

    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

        if (e == null)
            return;
        Log.i("VAL SELECTED",
                "Value: " + e.getVal() + ", xIndex: " + e.getXIndex()
                        + ", DataSet index: " + dataSetIndex);
    }

    @Override
    public void onNothingSelected() {
        Log.i("PieChart", "nothing selected");
    }


}
