package stocktrainer.com.transaction.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import stocktrainer.com.R;
import stocktrainer.com.Utility.ConnectionDetector;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.portfolio.adapter.PortfolioAdapter;
import stocktrainer.com.portfolio.fragment.PortfolioFragment;
import stocktrainer.com.portfolio.server.PortfolioGETRequest;
import stocktrainer.com.transaction.adapter.TransactionAdapter;
import stocktrainer.com.transaction.server.TransactionGETRequest;
import stocktrainer.com.watchlist.adapter.WatchlistAdapter;

public class TransactionsFragment extends Fragment {
    private Context mContext;
    View rootView;
    private ArrayList<JSONObject> lstTrasnsaction;
    private RecyclerView recyclerView;
    private ConnectionDetector cd;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_trasnsaction, container, false);
        mContext = getActivity();

        initComponent();

        return rootView;
    }

    private void initComponent(){
        cd = new ConnectionDetector(mContext);
        //set font style
        setFontStyle();
        lstTrasnsaction = new ArrayList<>();

        recyclerView = rootView.findViewById(R.id.rviewTransaction);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(new TransactionAdapter(mContext, lstTrasnsaction));
        (new TransactionAdapter(mContext, lstTrasnsaction)).notifyDataSetChanged();

    }

    @Override
    public void onResume() {
        super.onResume();
        if(cd.isConnectingToInternet()){
            getUserTransactionList();
        }else{
            Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);


        (activity.findViewById(R.id.imgvwHeaderRefresh)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Refreshing...",Toast.LENGTH_LONG).show();
                if(cd.isConnectingToInternet()){
                    getUserTransactionList();
                }else{
                    Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void getUserTransactionList() {
        new TransactionGETRequest(mContext,new TransactionGETRequest.TransactionGETRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj,AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if (status.getCode() == 200) {
                            try{
                                lstTrasnsaction.clear();
                                if(obj.getString("status").equals("true")){
                                    JSONArray jsonArrayWatchList = obj.getJSONArray("data");
                                    if(jsonArrayWatchList!=null && jsonArrayWatchList.length()>0){
                                        for (int i=0;i<jsonArrayWatchList.length();i++){
                                            JSONObject jObject = jsonArrayWatchList.getJSONObject(i);
                                            lstTrasnsaction.add(jObject);
                                        }
                                    }
                                    recyclerView.setVisibility(View.VISIBLE);
                                    (rootView.findViewById(R.id.txtContent0)).setVisibility(View.GONE);
                                    recyclerView.setAdapter(new TransactionAdapter(mContext, lstTrasnsaction));
                                    (new TransactionAdapter(mContext, lstTrasnsaction)).notifyDataSetChanged();
                                }else{
                                    recyclerView.setVisibility(View.GONE);
                                    (rootView.findViewById(R.id.txtContent0)).setVisibility(View.VISIBLE);
                                    ((TextView)rootView.findViewById(R.id.txtContent0)).setText(obj.getString("message"));
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }

    private void setFontStyle(){

        ((TextView)rootView.findViewById(R.id.txtTicker)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtEquityName)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtDate)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtGLPrice)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtType)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtNum)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtGLPercentage)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtPerSharePrice)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtTotal)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

    }
}
