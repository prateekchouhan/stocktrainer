package stocktrainer.com.transaction.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

import stocktrainer.com.R;
import stocktrainer.com.Utility.Utils;


public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.TransactionHolder> {
    ArrayList<JSONObject> lstPortfolio;
    Context mContext;

    public TransactionAdapter(Context context, ArrayList<JSONObject> lstPortfolio) {
        this.mContext = context;
        this.lstPortfolio = lstPortfolio;
    }

    @Override
    public TransactionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_transaction_item, parent, false);
        TransactionHolder imh = new TransactionHolder(v);
        return imh;
    }

    @Override
    public void onBindViewHolder(final TransactionHolder viewHolder, final int position) {
        try {
            //Set font style
            setFontStyle(viewHolder);

            viewHolder.txtSetTicker.setText(lstPortfolio.get(position).getString("dataset").split("/")[1]);
            viewHolder.txtSetEquityName.setText(lstPortfolio.get(position).getString("name"));
            viewHolder.txtSetDate.setText(lstPortfolio.get(position).getString("date_of_transaction").split(" ")[0]);
            viewHolder.txtSetGLPrice.setText("");
            viewHolder.txtType.setText(lstPortfolio.get(position).getString("type"));
            viewHolder.txtNum.setText(lstPortfolio.get(position).getString("quantity"));
            viewHolder.txtSetGLPercentage.setText("");
            viewHolder.txtTotal.setText(mContext.getString(R.string.Rs)+lstPortfolio.get(position).getString("total_price"));
            viewHolder.txtPerShareValue.setText(mContext.getString(R.string.Rs)+lstPortfolio.get(position).getString("buy_price"));

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (lstPortfolio != null) {
            return lstPortfolio.size();
        }
        return 0;
    }

    private void setFontStyle(TransactionHolder viewHolder){
        viewHolder.txtSetTicker.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtSetEquityName.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtSetDate.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtSetGLPrice.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtType.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtNum.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtSetGLPercentage.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtTotal.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtPerShareValue.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
    }

    public static class TransactionHolder extends RecyclerView.ViewHolder {
        TextView txtSetTicker,txtSetEquityName,txtSetDate, txtSetGLPrice, txtType,txtNum, txtSetGLPercentage,txtTotal,txtPerShareValue;
        TransactionHolder(View itemView) {
            super(itemView);
            txtSetTicker = itemView.findViewById(R.id.txtSetTicker);
            txtSetEquityName = itemView.findViewById(R.id.txtSetEquityName);
            txtSetDate = itemView.findViewById(R.id.txtSetDate);
            txtSetGLPrice = itemView.findViewById(R.id.txtSetGLPrice);
            txtType = itemView.findViewById(R.id.txtType);
            txtNum = itemView.findViewById(R.id.txtNum);
            txtSetGLPercentage = itemView.findViewById(R.id.txtSetGLPercentage);
            txtTotal = itemView.findViewById(R.id.txtTotal);
            txtPerShareValue = itemView.findViewById(R.id.txtPerShareValue);
        }
    }
}