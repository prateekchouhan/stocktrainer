package stocktrainer.com.challenge.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import stocktrainer.com.R;
import stocktrainer.com.Utility.ConnectionDetector;
import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.challenge.adapter.ChallengersAdapter;
import stocktrainer.com.challenge.server.ChallengeGETRequest;
import stocktrainer.com.find.stock.server.GetStockGETRequest;
import stocktrainer.com.top.movers.fragment.TopMoverFragment;


public class ChallengeFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    View rootView;
    private ArrayList<JSONObject> lstChallenge;
    private RecyclerView recyclerView;
    private ConnectionDetector cd;
    private int set_period=1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_challenge, container, false);

        mContext = this.getActivity();
        cd=new ConnectionDetector(mContext);

        initComponent();

        return rootView;
    }

    private void initComponent() {
        (rootView.findViewById(R.id.btnWeekly)).setOnClickListener(this);
        (rootView.findViewById(R.id.btnMonthly)).setOnClickListener(this);
        (rootView.findViewById(R.id.btnQuarterly)).setOnClickListener(this);

        (rootView.findViewById(R.id.btnWeekly)).setSelected(true);
        ((Button) rootView.findViewById(R.id.btnWeekly)).setTextColor(Color.WHITE);

        //set font style
        setFontStyle();
        lstChallenge = new ArrayList<>();

        recyclerView = rootView.findViewById(R.id.rviewChallenger);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(new ChallengersAdapter(mContext, lstChallenge));
        (new ChallengersAdapter(mContext, lstChallenge)).notifyDataSetChanged();

        if(cd.isConnectingToInternet()){
            getChallegeDetail("weekly");
        }else{
            Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);

        (activity.findViewById(R.id.imgvwHeaderSearch)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Refreshing...",Toast.LENGTH_LONG).show();
                if(cd.isConnectingToInternet()){
                    if(set_period==1){
                        getChallegeDetail("weekly");
                    }else if(set_period==2){
                        getChallegeDetail("quarterly");
                    }else{
                        getChallegeDetail("monthly");
                    }

                }else{
                    Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnWeekly:
                set_period=1;
                (rootView.findViewById(R.id.btnWeekly)).setSelected(true);
                (rootView.findViewById(R.id.btnMonthly)).setSelected(false);
                (rootView.findViewById(R.id.btnQuarterly)).setSelected(false);
                ((Button) rootView.findViewById(R.id.btnWeekly)).setTextColor(Color.WHITE);
                ((Button) rootView.findViewById(R.id.btnMonthly)).setTextColor(Color.BLACK);
                ((Button) rootView.findViewById(R.id.btnQuarterly)).setTextColor(Color.BLACK);
                if(cd.isConnectingToInternet()){
                    getChallegeDetail("weekly");
                }else{
                    Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnMonthly:
                set_period=3;
                (rootView.findViewById(R.id.btnWeekly)).setSelected(false);
                (rootView.findViewById(R.id.btnMonthly)).setSelected(true);
                (rootView.findViewById(R.id.btnQuarterly)).setSelected(false);
                ((Button) rootView.findViewById(R.id.btnWeekly)).setTextColor(Color.BLACK);
                ((Button) rootView.findViewById(R.id.btnMonthly)).setTextColor(Color.WHITE);
                ((Button) rootView.findViewById(R.id.btnQuarterly)).setTextColor(Color.BLACK);
                if(cd.isConnectingToInternet()){
                    getChallegeDetail("monthly");
                }else{
                    Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnQuarterly:
                set_period=2;
                (rootView.findViewById(R.id.btnWeekly)).setSelected(false);
                (rootView.findViewById(R.id.btnMonthly)).setSelected(false);
                (rootView.findViewById(R.id.btnQuarterly)).setSelected(true);
                ((Button) rootView.findViewById(R.id.btnWeekly)).setTextColor(Color.BLACK);
                ((Button) rootView.findViewById(R.id.btnMonthly)).setTextColor(Color.BLACK);
                ((Button) rootView.findViewById(R.id.btnQuarterly)).setTextColor(Color.WHITE);
                if(cd.isConnectingToInternet()){
                    getChallegeDetail("quarterly");
                }else{
                    Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
    }



    private void setFontStyle() {
        ((TextView) rootView.findViewById(R.id.txtUser)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) rootView.findViewById(R.id.txtGain)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) rootView.findViewById(R.id.txtPortfolioValue)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) rootView.findViewById(R.id.txtPortfolioUnit)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

        ((Button)rootView.findViewById(R.id.btnWeekly)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((Button)rootView.findViewById(R.id.btnMonthly)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((Button)rootView.findViewById(R.id.btnQuarterly)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

    }

    public void getChallegeDetail(String period) {

        new ChallengeGETRequest(mContext,period,new ChallengeGETRequest.ChallengeGETRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj,AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if (status.getCode() == 200) {
                            try{
                                lstChallenge.clear();
                                if(obj.getString("status").equals("true")){
                                    JSONArray jArray= obj.getJSONArray("data");
                                    if(jArray.length()>0 && jArray!=null){
                                        for (int i=0;i<jArray.length();i++){
                                            lstChallenge.add(jArray.getJSONObject(i));
                                        }
                                    }
                                    recyclerView.setAdapter(new ChallengersAdapter(mContext, lstChallenge));
                                    (new ChallengersAdapter(mContext, lstChallenge)).notifyDataSetChanged();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else if (status.getCode() == 401) {
                            Toast.makeText(mContext,mContext.getString(R.string.Invalide_Credential),Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }
}
