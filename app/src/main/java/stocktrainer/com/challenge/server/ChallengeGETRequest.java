package stocktrainer.com.challenge.server;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.constant.Constant;

public class ChallengeGETRequest {

	public static final String  BASE_URL = Constant.AppConstant.BASE_URL;
    public static final String  GET_CHALLENGE_URL = BASE_URL + Constant.APIMethods.GET_CHALLENGE;
    private ChallengeGETRequestCallback callback;

    public interface ChallengeGETRequestCallback {
        public void OnCompleteCallback(JSONObject obj, AjaxStatus status);
    }

    public ChallengeGETRequest(Context mContext, String period , ChallengeGETRequestCallback caller) {
        AQuery aq = new AQuery(mContext);
        this.callback = caller;

        aq.ajax(GET_CHALLENGE_URL+period, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jObject, AjaxStatus status) {
                try {
                    if(callback != null){
                        callback.OnCompleteCallback(jObject,status);
                    }else{
                        callback.OnCompleteCallback(null,null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.method(AQuery.METHOD_GET).header(Constant.AppConstant.AUTHORIZATION, PreferenceClass.getStringPreferences(mContext,Constant.User.AUTHORIZATION_TOKEN)));

    }
}
