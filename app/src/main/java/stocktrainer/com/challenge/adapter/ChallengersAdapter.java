package stocktrainer.com.challenge.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

import stocktrainer.com.R;
import stocktrainer.com.Utility.Utils;


public class ChallengersAdapter extends RecyclerView.Adapter<ChallengersAdapter.ChallengeHolder> {
    ArrayList<JSONObject> lstChallenge;
    Context mContext;

    public ChallengersAdapter(Context context, ArrayList<JSONObject> lstChallenge) {
        this.mContext = context;
        this.lstChallenge = lstChallenge;
    }

    @Override
    public ChallengeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_challenge_item, parent, false);
        ChallengeHolder imh = new ChallengeHolder(v);
        return imh;
    }

    @Override
    public void onBindViewHolder(final ChallengeHolder viewHolder, final int position) {
        try {
            //Set font style
            setFontStyle(viewHolder);

            if(lstChallenge.get(position).getString("name").trim().length()>0){
                viewHolder.txtSetUser.setText(lstChallenge.get(position).getString("name"));
            }else{
                viewHolder.txtSetUser.setText("Unknown");
            }

            viewHolder.txtSetGain.setText(lstChallenge.get(position).getString("diff_percent")+"%");

            double totalAmt = Double.valueOf(lstChallenge.get(position).getString("total_value"));
            float amtTotal = (float) totalAmt/100000;
            viewHolder.txtSetPortfolioValue.setText(""+amtTotal);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (lstChallenge != null) {
            return lstChallenge.size();
        }
        return 0;
    }

    private void setFontStyle(ChallengeHolder viewHolder){

        viewHolder.txtSetUser.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtSetGain.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtSetPortfolioValue.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

    }

    public static class ChallengeHolder extends RecyclerView.ViewHolder {
        TextView txtSetUser,txtSetGain,txtSetPortfolioValue;
        ChallengeHolder(View itemView) {
            super(itemView);
            txtSetUser = itemView.findViewById(R.id.txtSetUser);
            txtSetGain = itemView.findViewById(R.id.txtSetGain);
            txtSetPortfolioValue = itemView.findViewById(R.id.txtSetPortfolioValue);
        }
    }
}