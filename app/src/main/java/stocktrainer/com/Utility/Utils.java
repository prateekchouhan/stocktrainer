package stocktrainer.com.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Random;

/**
 * Created by mac on 13/09/17.
 */

public class Utils {

    public final static boolean isValidEmail(String email) {
        if (email == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public static Typeface setFontStyle(Context mContext, String style) {
        Typeface tfFontUI = null;
        //set regular style
        if (style.equals("Roboto_Regular")) {
            tfFontUI = Typeface.createFromAsset(mContext.getAssets(), "Roboto_Regular.ttf");
            return tfFontUI;
        }
        //set bold style
        if (style.equals("Roboto_Light")) {
            tfFontUI = Typeface.createFromAsset(mContext.getAssets(), "Roboto_Light.ttf");
            return tfFontUI;
        }

        //set medium style
        if (style.equals("Roboto_Medium")) {
            tfFontUI = Typeface.createFromAsset(mContext.getAssets(), "Roboto_Medium.ttf");
            return tfFontUI;
        }

        return tfFontUI;
    }

    public static void takeScreenshot(Activity act, View viewCapture) {

        try {
            String mPath = Environment.getExternalStorageDirectory().toString() + "/captured_screen.png";
            File imageFile = new File(mPath);
            deleteFile(imageFile);
            viewCapture.setDrawingCacheEnabled(true);
            Bitmap bitMapInvoice = Bitmap.createBitmap(viewCapture.getDrawingCache());
            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitMapInvoice.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();
            viewCapture.setDrawingCacheEnabled(false);
            System.out.println("imageFile path is: " + imageFile);
            shareImage(act, imageFile);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private static void shareImage(Activity act, File imageFile) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/jpg");
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile));
        act.startActivity(Intent.createChooser(shareIntent, "Share image using"));

        //
    }

    public static void deleteFile(File imageFile) {

        if (imageFile.exists()) {
            if (imageFile.delete()) {
                System.out.println("file Deleted :");
            } else {
                System.out.println("file not Deleted :");
            }
        }
    }

    public static int generatePin() throws Exception {
        Random generator = new Random();
        generator.setSeed(System.currentTimeMillis());

        int num = generator.nextInt(99999) + 99999;
        if (num < 100000 || num > 999999) {
            num = generator.nextInt(99999) + 99999;
            if (num < 100000 || num > 999999) {
                throw new Exception("Unable to generate PIN at this time..");
            }
        }
        return num;
    }

}
