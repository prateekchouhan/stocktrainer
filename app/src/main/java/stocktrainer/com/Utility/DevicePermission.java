package stocktrainer.com.Utility;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.File;



public class DevicePermission extends AppCompatActivity {
    public static final int MY_PERMISSIONS = 100;
    private Activity act;

    public DevicePermission(Activity activity) {
        this.act = activity;
        checkPermissions();
        Log.e("Inside","Device permission");
    }

    boolean checkPermissions() {
        Log.e("DevicePermission", "checkPermissions ");
        if (ActivityCompat.checkSelfPermission(act, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(act, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(act,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    },
                    MY_PERMISSIONS);
            Log.e("Inside","Checkpermission");

        } else {
           // createDirectry();
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS: {
                Log.e("Inside","My permission case");
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED
                        && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("***** DEVICE Permission Granted");

                } else {
                    System.out.println("Permission Denied");
                    finish();
                }
                return;
            }
        }
    }

}
