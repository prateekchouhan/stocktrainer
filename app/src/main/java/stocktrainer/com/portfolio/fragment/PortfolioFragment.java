package stocktrainer.com.portfolio.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;
import com.androidquery.callback.AjaxStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import stocktrainer.com.R;
import stocktrainer.com.Utility.ConnectionDetector;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.find.stock.StockDetailActivity;
import stocktrainer.com.find.stock.listener.RecyclerClickListner;
import stocktrainer.com.portfolio.StockDetailPortfolioActivity;
import stocktrainer.com.portfolio.adapter.PortfolioAdapter;
import stocktrainer.com.portfolio.server.PortfolioGETRequest;
import stocktrainer.com.watchlist.StockDetailWatchListActivity;
import stocktrainer.com.watchlist.fragment.WatchlistFragment;


public class PortfolioFragment extends Fragment implements RecyclerClickListner {
    private Context mContext;
    View rootView;
    private ArrayList<JSONObject> lstPortfolio;
    RecyclerView recyclerView;
    RecyclerClickListner mRecyclerClickListner;
    private ConnectionDetector cd;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_portfolio, container, false);
        mContext = getActivity();

        initComponent();

        return rootView;
    }

    private void initComponent(){
        cd = new ConnectionDetector(mContext);
        //set font style
        setFontStyle();
        lstPortfolio = new ArrayList<>();

        recyclerView = rootView.findViewById(R.id.rviewPortfolio);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(new PortfolioAdapter(mContext, lstPortfolio,mRecyclerClickListner));
        (new PortfolioAdapter(mContext, lstPortfolio,mRecyclerClickListner)).notifyDataSetChanged();


    }

    @Override
    public void onResume() {
        super.onResume();

        if(cd.isConnectingToInternet()){
            getPortfolioList();
        }else{
            Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        mRecyclerClickListner = PortfolioFragment.this;
        (activity.findViewById(R.id.imgvwHeaderRefresh)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Refreshing...",Toast.LENGTH_LONG).show();
                if(cd.isConnectingToInternet()){
                    getPortfolioList();
                }else{
                    Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void getPortfolioList() {

        new PortfolioGETRequest(mContext,new PortfolioGETRequest.PortfolioGETRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj,AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if (status.getCode() == 200) {
                            try{
                                lstPortfolio.clear();
                                if(obj.getString("status").equals("true")){
                                    JSONArray jsonArrayWatchList = obj.getJSONArray("data");
                                    if(jsonArrayWatchList!=null && jsonArrayWatchList.length()>0){
                                        for (int i=0;i<jsonArrayWatchList.length();i++){
                                            JSONObject jObject = jsonArrayWatchList.getJSONObject(i);
                                            lstPortfolio.add(jObject);
                                        }
                                    }
                                    recyclerView.setVisibility(View.VISIBLE);
                                    (rootView.findViewById(R.id.txtContent0)).setVisibility(View.GONE);
                                    recyclerView.setAdapter(new PortfolioAdapter(mContext, lstPortfolio,mRecyclerClickListner));
                                    (new PortfolioAdapter(mContext, lstPortfolio,mRecyclerClickListner)).notifyDataSetChanged();
                                }else{
                                    recyclerView.setVisibility(View.GONE);
                                    (rootView.findViewById(R.id.txtContent0)).setVisibility(View.VISIBLE);
                                    ((TextView)rootView.findViewById(R.id.txtContent0)).setText(obj.getString("message"));
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else if (status.getCode() == 401) {
                            Toast.makeText(mContext,mContext.getString(R.string.Invalide_Credential),Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }


    private void setFontStyle(){

        ((TextView)rootView.findViewById(R.id.txtContent0)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtTicker)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtPrice)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtValue)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtNum)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

        ((TextView)rootView.findViewById(R.id.txtEquityName)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));
        ((TextView)rootView.findViewById(R.id.txtValuePercentage)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));
        ((TextView)rootView.findViewById(R.id.txtNumUnit)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));
        ((TextView)rootView.findViewById(R.id.txtPriceUnite)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));

    }

    @Override
    public void onItemClick(View view, int position) {
        try {
            startActivity((new Intent(mContext, StockDetailPortfolioActivity.class)).putExtra("StockSelected",lstPortfolio.get(position).toString()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
