package stocktrainer.com.portfolio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

import stocktrainer.com.R;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.find.stock.listener.RecyclerClickListner;


public class PortfolioAdapter extends RecyclerView.Adapter<PortfolioAdapter.PortfolioHolder> {
    ArrayList<JSONObject> lstPortfolio;
    Context mContext;
    RecyclerClickListner mRecyclerClickListner;

    public PortfolioAdapter(Context context, ArrayList<JSONObject> lstPortfolio, RecyclerClickListner mRecyclerClickListner) {
        this.mContext = context;
        this.lstPortfolio = lstPortfolio;
        this.mRecyclerClickListner = mRecyclerClickListner;
    }

    @Override
    public PortfolioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_portfolio_item, parent, false);
        PortfolioHolder imh = new PortfolioHolder(v);
        return imh;
    }

    @Override
    public void onBindViewHolder(final PortfolioHolder viewHolder, final int position) {
        try {

            viewHolder.itemViewSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mRecyclerClickListner.onItemClick(viewHolder.itemViewSelected,position);
                }
            });

            //Set font style
            setFontStyle(viewHolder);

            viewHolder.txtSetTicker.setText(lstPortfolio.get(position).getString("dataset_code"));
            viewHolder.txtSetEquityName.setText(lstPortfolio.get(position).getString("name"));
            viewHolder.txtSetPrice.setText(mContext.getString(R.string.Rs)+lstPortfolio.get(position).getString("current_price"));
            viewHolder.txtSetPriceUnite.setText(mContext.getString(R.string.Rs)+lstPortfolio.get(position).getString("gain_loss"));
            viewHolder.txtSetNum.setText(lstPortfolio.get(position).getString("total_quantity"));
            viewHolder.txtSetNumUnit.setText(mContext.getString(R.string.Rs)+lstPortfolio.get(position).getString("price_change"));
            viewHolder.txtSetValue.setText(mContext.getString(R.string.Rs)+lstPortfolio.get(position).getString("value"));
            viewHolder.txtSetValuePercentage.setText(lstPortfolio.get(position).getString("percent_change")+"%");


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (lstPortfolio != null) {
            return lstPortfolio.size();
        }
        return 0;
    }

    private void setFontStyle(PortfolioHolder viewHolder){
        viewHolder.txtSetTicker.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtSetPrice.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtSetNum.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtSetValue.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

        viewHolder.txtSetEquityName.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        viewHolder.txtSetPriceUnite.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        viewHolder.txtSetNumUnit.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        viewHolder.txtSetValuePercentage.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
    }

    public static class PortfolioHolder extends RecyclerView.ViewHolder {
        TextView txtSetTicker,txtSetPrice,txtSetNum,txtSetValue,txtSetEquityName,txtSetPriceUnite,txtSetNumUnit,txtSetValuePercentage;
        private View itemViewSelected;
        PortfolioHolder(View itemView) {
            super(itemView);
            itemViewSelected=itemView;
            txtSetTicker = itemView.findViewById(R.id.txtSetTicker);
            txtSetPrice = itemView.findViewById(R.id.txtSetPrice);
            txtSetNum = itemView.findViewById(R.id.txtSetNum);
            txtSetValue = itemView.findViewById(R.id.txtSetValue);
            txtSetEquityName = itemView.findViewById(R.id.txtSetEquityName);
            txtSetPriceUnite = itemView.findViewById(R.id.txtSetPriceUnite);
            txtSetNumUnit = itemView.findViewById(R.id.txtSetNumUnit);
            txtSetValuePercentage = itemView.findViewById(R.id.txtSetValuePercentage);
        }
    }
}