package stocktrainer.com.auth.server;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.Map;

import stocktrainer.com.constant.Constant;

public class ForgotPasswordPOSTRequest {

	public static final String  BASE_URL = Constant.AppConstant.BASE_URL;
    public static final String  FORGOT_PASSWORD_URL = BASE_URL + Constant.APIMethods.FORGOT_PASSWORD;

    private static final String TAG = ForgotPasswordPOSTRequest.class.getSimpleName();
    private ForgotPasswordPOSTRequestCallback callback;

    public interface ForgotPasswordPOSTRequestCallback {
        public void OnCompleteCallback(JSONObject obj, AjaxStatus status);
    }

    public ForgotPasswordPOSTRequest(Context mContext, Map<String, Object> params , ForgotPasswordPOSTRequestCallback caller) {
        AQuery aq = new AQuery(mContext);
        this.callback = caller;

        aq.ajax(FORGOT_PASSWORD_URL, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jObject, AjaxStatus status) {
                try {
                    if(callback != null){
                        callback.OnCompleteCallback(jObject,status);
                    }else{
                        callback.OnCompleteCallback(null,null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.method(AQuery.METHOD_POST));

    }

}
