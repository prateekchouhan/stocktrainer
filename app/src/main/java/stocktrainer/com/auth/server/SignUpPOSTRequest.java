package stocktrainer.com.auth.server;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.Map;

import stocktrainer.com.constant.Constant;

public class SignUpPOSTRequest {

	public static final String  BASE_URL = Constant.AppConstant.BASE_URL;
    public static final String  SIGN_UP_URL = BASE_URL + Constant.APIMethods.SIGNUP;

    private static final String TAG = SignUpPOSTRequest.class.getSimpleName();
    private SignUpPOSTRequestCallback callback;

    public interface SignUpPOSTRequestCallback {
        public void OnCompleteCallback(JSONObject obj, AjaxStatus status);
    }

    public SignUpPOSTRequest(Context mContext, Map<String, Object> params , SignUpPOSTRequestCallback caller) {
        AQuery aq = new AQuery(mContext);
        this.callback = caller;

        aq.ajax(SIGN_UP_URL, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jObject, AjaxStatus status) {
                try {
                    if(callback != null){
                        callback.OnCompleteCallback(jObject,status);
                    }else{
                        callback.OnCompleteCallback(null,null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.method(AQuery.METHOD_POST));

    }
}
