package stocktrainer.com.auth.server;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import stocktrainer.com.R;
import stocktrainer.com.constant.Constant;

public class LoginPOSTRequest {

	public static final String  BASE_URL = Constant.AppConstant.BASE_URL;
    public static final String  LOGIN_URL = BASE_URL + Constant.APIMethods.LOGIN;

    private static final String TAG = LoginPOSTRequest.class.getSimpleName();
    private LoginPOSTRequestCallback callback;

    public interface LoginPOSTRequestCallback {
        public void OnCompleteCallback(JSONObject obj,AjaxStatus status);
    }

    public LoginPOSTRequest(Context mContext, Map<String, Object> params , LoginPOSTRequestCallback caller) {
        AQuery aq = new AQuery(mContext);
        this.callback = caller;

        aq.ajax(LOGIN_URL, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jObject, AjaxStatus status) {
                try {
                    if(callback != null){
                        callback.OnCompleteCallback(jObject,status);
                    }else{
                        callback.OnCompleteCallback(null,null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.method(AQuery.METHOD_POST));

    }
}
