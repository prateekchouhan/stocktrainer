package stocktrainer.com.auth;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import stocktrainer.com.R;
import stocktrainer.com.Utility.ConnectionDetector;
import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.auth.server.ForgotPasswordPOSTRequest;
import stocktrainer.com.auth.server.LoginPOSTRequest;
import stocktrainer.com.constant.Constant;
import stocktrainer.com.dashboard.MainActivity;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener{
    private Context mContext;
    private ConnectionDetector cd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mContext=this;

        initComponent();

    }





    private void initComponent(){
        cd=new ConnectionDetector(mContext);

               //call font style
        setFontStyle();

        //call click listener
        setClickListener();
    }

    private void setFontStyle(){

        ((EditText)findViewById(R.id.edtMobile)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));
        ((Button)findViewById(R.id.btnSubmit)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtGetStarted)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

    }

    private void setClickListener(){
        (findViewById(R.id.btnSubmit)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                String strMobile = ((EditText)findViewById(R.id.edtMobile)).getText().toString().trim();

                if(strMobile.length()==0){
                    Toast.makeText(mContext,mContext.getString(R.string.email_blank),Toast.LENGTH_LONG).show();
                }else if(strMobile.length()!=10){
                    Toast.makeText(mContext,mContext.getString(R.string.mobile_length),Toast.LENGTH_LONG).show();
                }else{
                    if(cd.isConnectingToInternet()){
                        (findViewById(R.id.progressBar)).setVisibility(View.VISIBLE);
                        forgotPassword(strMobile);
                    }else {
                        Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                    }
                }
                break;

            default:
                break;
        }
    }

    public void forgotPassword(final String mobile) {

        Map<String, Object> params = new HashMap<>();
        params.put("mobile", mobile);

        new ForgotPasswordPOSTRequest(mContext,params,new ForgotPasswordPOSTRequest.ForgotPasswordPOSTRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj,AjaxStatus status) {
                (findViewById(R.id.progressBar)).setVisibility(View.GONE);
                if (status.getCode() == 200) {
                    try{
                        if(obj.getString("status").equals("true")){
                            Toast.makeText(mContext,mContext.getString(R.string.Password_Sent),Toast.LENGTH_LONG).show();
                            finish();
                        }else{
                            Toast.makeText(mContext,obj.getString("message"),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }
}
