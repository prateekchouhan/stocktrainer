package stocktrainer.com.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.androidquery.callback.AjaxStatus;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import stocktrainer.com.R;
import stocktrainer.com.Utility.ConnectionDetector;
import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.auth.server.OTPPOSTRequest;
import stocktrainer.com.constant.Constant;
import stocktrainer.com.dashboard.MainActivity;


public class OTPValidateActivity extends AppCompatActivity implements View.OnClickListener{
    private Context mContext;
    private ConnectionDetector cd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_validate);
        mContext=this;
        initComponent();
    }

    private void initComponent(){
        cd=new ConnectionDetector(mContext);
        if(PreferenceClass.getStringPreferences(mContext,Constant.User.MOBILE_OTP).trim().length()>0){
            ((EditText)findViewById(R.id.edtOTP)).setText(PreferenceClass.getStringPreferences(mContext,Constant.User.MOBILE_OTP));
        }else{
            ((EditText)findViewById(R.id.edtOTP)).setText("");
        }

        ((EditText)findViewById(R.id.edtOTP)).setSelection(((EditText)findViewById(R.id.edtOTP)).getText().toString().trim().length());
        //call font style
        setFontStyle();

        //call click listener
        setClickListener();
    }

    private void setFontStyle(){
        ((TextView)findViewById(R.id.txtResendOTP)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));
        ((EditText)findViewById(R.id.edtOTP)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));
        ((Button)findViewById(R.id.btnSubmit)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

    }

    private void setClickListener(){
        (findViewById(R.id.btnSubmit)).setOnClickListener(this);
        (findViewById(R.id.txtResendOTP)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                String strOTP = ((EditText)findViewById(R.id.edtOTP)).getText().toString().trim();
                if(strOTP.length()==0){
                    Toast.makeText(mContext,mContext.getString(R.string.otp_blank),Toast.LENGTH_LONG).show();
                }else{
                    if(cd.isConnectingToInternet()){
                        (findViewById(R.id.progressBar)).setVisibility(View.VISIBLE);
                        verifyOTP(strOTP);
                    }else {
                        Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case R.id.txtResendOTP:
                Toast.makeText(mContext,"In Process...",Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }

    public void verifyOTP(String opt) {

        Map<String, Object> params = new HashMap<>();
        params.put("otp", opt);

        new OTPPOSTRequest(mContext,params,new OTPPOSTRequest.OTPPOSTRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj,AjaxStatus status) {
                (findViewById(R.id.progressBar)).setVisibility(View.GONE);
                if (status.getCode() == 200) {
                    try {
                        if(obj.getString("status").equals("true")){
                            startActivity(new Intent(mContext,MainActivity.class));
                            finish();
                        }else{
                            Toast.makeText(mContext,obj.getString("message"),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else if (status.getCode() == 401) {
                    Toast.makeText(mContext,mContext.getString(R.string.Invalide_Credential),Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }
}
