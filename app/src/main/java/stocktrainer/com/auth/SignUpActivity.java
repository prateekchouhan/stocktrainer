package stocktrainer.com.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.androidquery.callback.AjaxStatus;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import stocktrainer.com.R;
import stocktrainer.com.Utility.ConnectionDetector;
import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.auth.server.SignUpPOSTRequest;
import stocktrainer.com.constant.Constant;
import stocktrainer.com.dashboard.MainActivity;


public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{
    private Context mContext;
    private ConnectionDetector cd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mContext=this;

        initComponent();

    }


    private void initComponent(){
        cd=new ConnectionDetector(mContext);
        //call font style
        setFontStyle();

        //call click listener
        setClickListener();
    }

    private void setFontStyle(){

        ((TextView)findViewById(R.id.txtSkip)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));
        ((TextView)findViewById(R.id.txtExistUser)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));


        ((EditText)findViewById(R.id.edtMobile)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));
        ((EditText)findViewById(R.id.edtPassword)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));
        ((EditText)findViewById(R.id.edtConfirmPassword)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Light"));


        ((Button)findViewById(R.id.btnSubmit)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtGetStarted)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

    }

    private void setClickListener(){
        (findViewById(R.id.btnSubmit)).setOnClickListener(this);
        //(findViewById(R.id.txtSkip)).setOnClickListener(this);
        (findViewById(R.id.txtExistUser)).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                String strMobile = ((EditText)findViewById(R.id.edtMobile)).getText().toString().trim();
                String strPassword = ((EditText)findViewById(R.id.edtPassword)).getText().toString().trim();
                String strConPassword = ((EditText)findViewById(R.id.edtConfirmPassword)).getText().toString().trim();

                if(strMobile.length()==0){
                    Toast.makeText(mContext,mContext.getString(R.string.mobile_blank),Toast.LENGTH_LONG).show();
                }else if(strMobile.length()!=10){
                    Toast.makeText(mContext,mContext.getString(R.string.mobile_length),Toast.LENGTH_LONG).show();
                }else if(strPassword.length()==0){
                    Toast.makeText(mContext,mContext.getString(R.string.password_blank),Toast.LENGTH_LONG).show();
                }else if(!strConPassword.equals(strPassword)){
                    Toast.makeText(mContext,mContext.getString(R.string.password_not_match),Toast.LENGTH_LONG).show();
                }else{
                    if(cd.isConnectingToInternet()){
                        (findViewById(R.id.progressBar)).setVisibility(View.VISIBLE);
                        sigupUser(strMobile,strPassword);
                    }else {
                        Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case R.id.txtSkip:
                startActivity(new Intent(mContext,MainActivity.class));
                finish();
                break;
            case R.id.txtExistUser:
                finish();
                break;
            default:
                break;
        }
    }

    public void sigupUser(final String mobile,String password) {
        Map<String, Object> params = new HashMap<>();
        params.put("mobile", mobile);
        params.put("password", password);
        params.put("device_type", Constant.AppConstant.ANDROID);
        params.put("device_token", PreferenceClass.getStringPreferences(mContext,Constant.AppConstant.FCM_TOKEN));

        new SignUpPOSTRequest(mContext,params,new SignUpPOSTRequest.SignUpPOSTRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj,AjaxStatus status) {
                try{
                    (findViewById(R.id.progressBar)).setVisibility(View.GONE);
                    if (status.getCode() == 200) {
                        if(obj.getString("status").equals("true")){
                            PreferenceClass.setStringPreference(mContext,Constant.User.USER_NAME,obj.getString("name"));
                            PreferenceClass.setStringPreference(mContext,Constant.User.USER_EMAIL,obj.getString("email"));
                            PreferenceClass.setStringPreference(mContext,Constant.User.USER_ID,obj.getString("user_id"));
                            PreferenceClass.setStringPreference(mContext,Constant.User.AUTHORIZATION_TOKEN,obj.getString("token"));
                            PreferenceClass.setStringPreference(mContext,Constant.User.MOBILE_NUMBER,mobile);
                            PreferenceClass.setStringPreference(mContext,Constant.User.MOBILE_OTP,obj.getString("otp"));
                            startActivity(new Intent(mContext,OTPValidateActivity.class));
                            finish();
                        }else{
                            Toast.makeText(mContext,obj.getString("message"),Toast.LENGTH_LONG).show();
                        }
                    }else {
                        Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        } );

    }
}
