package stocktrainer.com.top.movers.server;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.constant.Constant;

public class TopGainerGETRequest {

	public static final String  BASE_URL = Constant.AppConstant.BASE_URL;
    public static final String  TOP_GAINER_URL = BASE_URL + Constant.APIMethods.TOP_GAINER;
    private TopGainerGETRequestCallback callback;

    public interface TopGainerGETRequestCallback {
        public void OnCompleteCallback(JSONObject obj, AjaxStatus status);
    }

    public TopGainerGETRequest(Context mContext, TopGainerGETRequestCallback caller) {
        AQuery aq = new AQuery(mContext);
        this.callback = caller;

        aq.ajax(TOP_GAINER_URL, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jObject, AjaxStatus status) {
                try {
                    if(callback != null){
                        System.out.println("jObject Detailsss isss: "+jObject);
                        callback.OnCompleteCallback(jObject,status);
                    }else{
                        callback.OnCompleteCallback(null,null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.method(AQuery.METHOD_GET).header(Constant.AppConstant.AUTHORIZATION, PreferenceClass.getStringPreferences(mContext,Constant.User.AUTHORIZATION_TOKEN)));
    }
}
