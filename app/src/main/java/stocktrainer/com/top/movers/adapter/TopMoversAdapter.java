package stocktrainer.com.top.movers.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

import stocktrainer.com.R;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.find.stock.listener.RecyclerClickListner;


public class TopMoversAdapter extends RecyclerView.Adapter<TopMoversAdapter.TopMoverHolder> {
    ArrayList<JSONObject> lstPortfolio;
    Context mContext;
    RecyclerClickListner mRecyclerClickListner;
    public TopMoversAdapter(Context context, ArrayList<JSONObject> lstPortfolio,RecyclerClickListner mRecyclerClickListner) {
        this.mContext = context;
        this.lstPortfolio = lstPortfolio;
        this.mRecyclerClickListner = mRecyclerClickListner;
    }

    @Override
    public TopMoverHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_top_movers_item, parent, false);
        TopMoverHolder imh = new TopMoverHolder(v);
        return imh;
    }

    @Override
    public void onBindViewHolder(final TopMoverHolder viewHolder, final int position) {
        try {
            //Set font style
            setFontStyle(viewHolder);

            viewHolder.itemViewSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mRecyclerClickListner.onItemClick(viewHolder.itemViewSelected,position);
                }
            });


            viewHolder.txtSetEquityName.setText(lstPortfolio.get(position).getString("code"));
            viewHolder.txtSetEquCompanyName.setText(lstPortfolio.get(position).getString("name"));
            viewHolder.txtSetVolume.setText("0.0");
            viewHolder.txtSetVolume1.setText(lstPortfolio.get(position).getString("price"));
            viewHolder.txtSetVolume1Percentage.setText(lstPortfolio.get(position).getString("percent_change")+"%");


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (lstPortfolio != null) {
            return lstPortfolio.size();
        }
        return 0;
    }

    private void setFontStyle(TopMoverHolder viewHolder){

        viewHolder.txtSetEquityName.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        viewHolder.txtSetEquCompanyName.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        viewHolder.txtSetVolume.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        viewHolder.txtSetVolume1.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        viewHolder.txtSetVolume1Percentage.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));

    }

    public static class TopMoverHolder extends RecyclerView.ViewHolder {
        TextView txtSetEquityName,txtSetEquCompanyName,txtSetVolume,txtSetVolume1,txtSetVolume1Percentage;
        private View itemViewSelected;
        TopMoverHolder(View itemView) {
            super(itemView);
            itemViewSelected=itemView;
            txtSetEquityName = itemView.findViewById(R.id.txtSetEquityName);
            txtSetEquCompanyName = itemView.findViewById(R.id.txtSetEquCompanyName);
            txtSetVolume = itemView.findViewById(R.id.txtSetVolume);
            txtSetVolume1 = itemView.findViewById(R.id.txtSetVolume1);
            txtSetEquityName = itemView.findViewById(R.id.txtSetEquityName);
            txtSetVolume1Percentage = itemView.findViewById(R.id.txtSetVolume1Percentage);
        }
    }
}