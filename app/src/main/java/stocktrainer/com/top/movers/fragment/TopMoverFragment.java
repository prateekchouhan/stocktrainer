package stocktrainer.com.top.movers.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import stocktrainer.com.R;
import stocktrainer.com.Utility.ConnectionDetector;
import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.find.stock.StockDetailActivity;
import stocktrainer.com.find.stock.fragment.FindStockFragment;
import stocktrainer.com.find.stock.listener.RecyclerClickListner;
import stocktrainer.com.find.stock.server.GetStockGETRequest;
import stocktrainer.com.portfolio.StockDetailPortfolioActivity;
import stocktrainer.com.portfolio.adapter.PortfolioAdapter;
import stocktrainer.com.top.movers.StockDetailTopMoversActivity;
import stocktrainer.com.top.movers.adapter.TopMoversAdapter;
import stocktrainer.com.top.movers.server.TopGainerGETRequest;
import stocktrainer.com.top.movers.server.TopLoserGETRequest;

public class TopMoverFragment extends Fragment implements View.OnClickListener,RecyclerClickListner{

    private Context mContext;
    View rootView;
    private ArrayList<JSONObject> lstTopGainerLoser;

    private RecyclerView recyclerView;
    private  RecyclerClickListner mRecyclerClickListner;

    private ConnectionDetector cd ;
    private boolean isGainer=true;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_top_mover, container, false);

        mContext=this.getActivity();

        initComponent();

        return rootView;
    }

    private void initComponent(){
        (rootView.findViewById(R.id.btnGainer)).setOnClickListener(this);
        (rootView.findViewById(R.id.btnLosser)).setOnClickListener(this);

        (rootView.findViewById(R.id.btnGainer)).setSelected(true);
        ((Button)rootView.findViewById(R.id.btnGainer)).setTextColor(Color.WHITE);

        //set font style
        setFontStyle();
        lstTopGainerLoser = new ArrayList<>();

        recyclerView = rootView.findViewById(R.id.rviewTopMovers);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(new TopMoversAdapter(mContext, lstTopGainerLoser,mRecyclerClickListner));
        (new TopMoversAdapter(mContext, lstTopGainerLoser,mRecyclerClickListner)).notifyDataSetChanged();
        cd = new ConnectionDetector(mContext);
        if(cd.isConnectingToInternet()){
            topGainerDetail();
        }else{
            Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnGainer:
                isGainer=true;
                (rootView.findViewById(R.id.btnGainer)).setSelected(true);
                (rootView.findViewById(R.id.btnLosser)).setSelected(false);
                ((Button)rootView.findViewById(R.id.btnGainer)).setTextColor(Color.WHITE);
                ((Button)rootView.findViewById(R.id.btnLosser)).setTextColor(Color.BLACK);
                lstTopGainerLoser.clear();
                (new TopMoversAdapter(mContext, lstTopGainerLoser,mRecyclerClickListner)).notifyDataSetChanged();
                if(cd.isConnectingToInternet()){
                    topGainerDetail();
                }else{
                    Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnLosser:
                isGainer=false;
                (rootView.findViewById(R.id.btnGainer)).setSelected(false);
                (rootView.findViewById(R.id.btnLosser)).setSelected(true);
                ((Button)rootView.findViewById(R.id.btnGainer)).setTextColor(Color.BLACK);
                ((Button)rootView.findViewById(R.id.btnLosser)).setTextColor(Color.WHITE);
                lstTopGainerLoser.clear();
                (new TopMoversAdapter(mContext, lstTopGainerLoser,mRecyclerClickListner)).notifyDataSetChanged();
                if(cd.isConnectingToInternet()){
                    topLoserDetail();
                }else{
                    Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        mRecyclerClickListner = TopMoverFragment.this;

        (activity.findViewById(R.id.imgvwHeaderRefresh)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Refreshing...",Toast.LENGTH_LONG).show();
                if(isGainer){
                    if(cd.isConnectingToInternet()){
                        topGainerDetail();
                    }else{
                        Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                    }
                }else{
                    if(cd.isConnectingToInternet()){
                        topLoserDetail();
                    }else{
                        Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


    }

    private void setFontStyle(){
        ((TextView)rootView.findViewById(R.id.txtEquityName)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)rootView.findViewById(R.id.txtVolume)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));

    }

    public void topGainerDetail() {

        new TopGainerGETRequest(mContext,new TopGainerGETRequest.TopGainerGETRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj, AjaxStatus status) {
                try{
                    if(status.getCode()==200){
                        lstTopGainerLoser.clear();
                        if(obj.getString("status").equals("true")){
                            JSONArray jsonArray= obj.getJSONArray("data");
                            if(jsonArray!=null && jsonArray.length()>0){
                                for (int i=0;i<jsonArray.length();i++){
                                    lstTopGainerLoser.add(jsonArray.getJSONObject(i));
                                }

                            }
                            recyclerView.setAdapter(new TopMoversAdapter(mContext, lstTopGainerLoser,mRecyclerClickListner));
                            (new TopMoversAdapter(mContext, lstTopGainerLoser,mRecyclerClickListner)).notifyDataSetChanged();

                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    public void topLoserDetail() {

        new TopLoserGETRequest(mContext,new TopLoserGETRequest.TopLoserGETRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj, AjaxStatus status) {
                try{
                    if(status.getCode()==200){
                        if(obj.getString("status").equals("true")){
                            JSONArray jsonArray= obj.getJSONArray("data");
                            if(jsonArray!=null && jsonArray.length()>0){
                                for (int i=0;i<jsonArray.length();i++){
                                    lstTopGainerLoser.add(jsonArray.getJSONObject(i));
                                }

                            }
                            recyclerView.setAdapter(new TopMoversAdapter(mContext, lstTopGainerLoser,mRecyclerClickListner));
                            (new TopMoversAdapter(mContext, lstTopGainerLoser,mRecyclerClickListner)).notifyDataSetChanged();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        try {
            startActivity((new Intent(mContext, StockDetailTopMoversActivity.class)).putExtra("StockSelected",lstTopGainerLoser.get(position).toString()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
