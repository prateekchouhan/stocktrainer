package stocktrainer.com.find.stock.server;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.Map;

import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.constant.Constant;

public class StockStatsPOSTRequest {

	public static final String  BASE_URL = Constant.AppConstant.BASE_URL;
    public static final String  STOCK_STATS_URL = BASE_URL + Constant.APIMethods.STOCK_STATS;
    private StockStatsPOSTRequestCallback callback;

    public interface StockStatsPOSTRequestCallback {
        public void OnCompleteCallback(JSONObject obj, AjaxStatus status);
    }

    public StockStatsPOSTRequest(Context mContext, Map<String, Object> params , StockStatsPOSTRequestCallback caller) {
        AQuery aq = new AQuery(mContext);
        this.callback = caller;

        aq.ajax(STOCK_STATS_URL, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jObject, AjaxStatus status) {
                try {
                    if(callback != null){
                        callback.OnCompleteCallback(jObject,status);
                    }else{
                        callback.OnCompleteCallback(null,null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.method(AQuery.METHOD_POST).header(Constant.AppConstant.AUTHORIZATION, PreferenceClass.getStringPreferences(mContext,Constant.User.AUTHORIZATION_TOKEN)));

    }
}
