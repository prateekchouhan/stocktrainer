package stocktrainer.com.find.stock.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import stocktrainer.com.R;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.find.stock.listener.RecyclerClickListner;
import stocktrainer.com.find.stock.StockDetailActivity;
import stocktrainer.com.find.stock.adapter.FindStockAdapter;
import stocktrainer.com.find.stock.server.FindStockPOSTRequest;

/**
 * Created by mac on 11/09/17.
 */

public class FindStockFragment extends Fragment implements RecyclerClickListner {
    private Context mContext;
    View rootView;
    private ArrayList<JSONObject> lstFindStock;
    RecyclerView recyclerView;
    RecyclerClickListner mRecyclerClickListner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_find_stock, container, false);
        mContext = getActivity();

        initComponent();

        return rootView;
    }

    private void initComponent(){
        lstFindStock = new ArrayList<>();

        recyclerView = rootView.findViewById(R.id.rviewFindStock);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(new FindStockAdapter(mContext, lstFindStock,mRecyclerClickListner));
        (new FindStockAdapter(mContext, lstFindStock,mRecyclerClickListner)).notifyDataSetChanged();

        //set font style.
        setFontStyle();
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        mRecyclerClickListner = FindStockFragment.this;

        (activity.findViewById(R.id.imgvwHeaderSearch)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                (activity.findViewById(R.id.edtSearch)).requestFocus();
                (activity.findViewById(R.id.edtSearch)).setFocusableInTouchMode(true);

                InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput((activity.findViewById(R.id.edtSearch)), InputMethodManager.SHOW_FORCED);
                visibleHideHeaderView(activity);
            }
        });


    }

    private void visibleHideHeaderView(Activity activity){
        (activity.findViewById(R.id.txtTitleToolbar)).setVisibility(View.GONE);
        (activity.findViewById(R.id.imgvwHeaderCamera)).setVisibility(View.GONE);
        (activity.findViewById(R.id.imgvwHeaderRefresh)).setVisibility(View.GONE);
        (activity.findViewById(R.id.imgvwHeaderSearch)).setVisibility(View.GONE);
        (activity.findViewById(R.id.edtSearch)).setVisibility(View.VISIBLE);
        ((EditText)activity.findViewById(R.id.edtSearch)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.toString().trim().length()>2){
                    recyclerView.setVisibility(View.VISIBLE);
                    (rootView.findViewById(R.id.layoutNOSearchStock)).setVisibility(View.GONE);
                    findStock(charSequence.toString().trim());
                }else if(charSequence.toString().trim().length()==0){
                    lstFindStock.clear();
                    recyclerView.setVisibility(View.GONE);
                    (rootView.findViewById(R.id.layoutNOSearchStock)).setVisibility(View.VISIBLE);
                    recyclerView.setAdapter(new FindStockAdapter(mContext, lstFindStock,mRecyclerClickListner));
                    (new FindStockAdapter(mContext, lstFindStock,mRecyclerClickListner)).notifyDataSetChanged();
                }else{
                    recyclerView.setVisibility(View.VISIBLE);
                    (rootView.findViewById(R.id.layoutNOSearchStock)).setVisibility(View.GONE);
                    recyclerView.setAdapter(new FindStockAdapter(mContext, lstFindStock,mRecyclerClickListner));
                    (new FindStockAdapter(mContext, lstFindStock,mRecyclerClickListner)).notifyDataSetChanged();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    private void setFontStyle(){
        ((TextView)rootView.findViewById(R.id.txtContent0)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtContent1)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtContent2)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)rootView.findViewById(R.id.txtContent3)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

        recyclerView.setVisibility(View.GONE);
        (rootView.findViewById(R.id.layoutNOSearchStock)).setVisibility(View.VISIBLE);

    }

    public void findStock(String search_keyword) {

        Map<String, Object> params = new HashMap<>();
        params.put("keyword", search_keyword);

        new FindStockPOSTRequest(mContext,params,new FindStockPOSTRequest.FindStockPOSTRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj,AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if(obj.getString("status").equals("true")){
                            JSONArray jsonArrayStock = obj.getJSONArray("data");
                            lstFindStock.clear();
                            if(jsonArrayStock.length()>0 && jsonArrayStock!=null){
                                for (int i=0;i<jsonArrayStock.length();i++){
                                    JSONObject jObject=jsonArrayStock.getJSONObject(i);
                                    lstFindStock.add(jObject);
                                }
                                recyclerView.setVisibility(View.VISIBLE);
                                (rootView.findViewById(R.id.layoutNOSearchStock)).setVisibility(View.GONE);
                                recyclerView.setAdapter(new FindStockAdapter(mContext, lstFindStock,mRecyclerClickListner));
                                (new FindStockAdapter(mContext, lstFindStock,mRecyclerClickListner)).notifyDataSetChanged();
                            }else{
                                recyclerView.setVisibility(View.GONE);
                                (rootView.findViewById(R.id.layoutNOSearchStock)).setVisibility(View.VISIBLE);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }


            }
        } );
    }



    @Override
    public void onItemClick(View view, int position) {
        try {
            startActivity((new Intent(mContext, StockDetailActivity.class)).putExtra("FindStockSelected",lstFindStock.get(position).toString()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
