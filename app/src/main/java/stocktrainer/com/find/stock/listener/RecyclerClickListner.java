package stocktrainer.com.find.stock.listener;

import android.view.View;

/**
 * Created by user on 16/12/16.
 */

public interface RecyclerClickListner {
    void onItemClick(View view, int position);
}
