package stocktrainer.com.find.stock.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

import stocktrainer.com.R;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.find.stock.listener.RecyclerClickListner;


public class FindStockAdapter extends RecyclerView.Adapter<FindStockAdapter.FindStockHolder> {
    ArrayList<JSONObject> lstFindStock;
    Context mContext;
    RecyclerClickListner mRecyclerClickListner;

    public FindStockAdapter(Context context, ArrayList<JSONObject> lstFindStock,RecyclerClickListner mRecyclerClickListner) {
        this.mContext = context;
        this.lstFindStock = lstFindStock;
        this.mRecyclerClickListner = mRecyclerClickListner;
    }

    @Override
    public FindStockHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_find_stock_item, parent, false);
        FindStockHolder imh = new FindStockHolder(v);
        return imh;
    }

    @Override
    public void onBindViewHolder(final FindStockHolder viewHolder, final int position) {
        try {
            //Set font style
            setFontStyle(viewHolder);

            viewHolder.itemViewSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mRecyclerClickListner.onItemClick(viewHolder.itemViewSelected,position);
                }
            });

            viewHolder.txtSetStockName.setText(lstFindStock.get(position).getString("name"));
            viewHolder.txtSetStockSortName.setText(lstFindStock.get(position).getString("code"));
            viewHolder.txtSetStockType.setText(lstFindStock.get(position).getString("exchange"));

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (lstFindStock != null) {
            return lstFindStock.size();
        }
        return 0;
    }

    private void setFontStyle(FindStockHolder viewHolder){
        viewHolder.txtSetStockName.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

        viewHolder.txtSetStockType.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        viewHolder.txtSetStockSortName.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
    }

    public static class FindStockHolder extends RecyclerView.ViewHolder {
        private View itemViewSelected;
        TextView txtSetStockName,txtSetStockSortName,txtSetStockType;
        FindStockHolder(View itemView) {
            super(itemView);
            itemViewSelected=itemView;
            txtSetStockName = itemView.findViewById(R.id.txtSetStockName);
            txtSetStockSortName = itemView.findViewById(R.id.txtSetStockSortName);
            txtSetStockType = itemView.findViewById(R.id.txtSetStockType);
        }
    }
}