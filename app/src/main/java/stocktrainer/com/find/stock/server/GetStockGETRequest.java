package stocktrainer.com.find.stock.server;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.Map;

import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.constant.Constant;

public class GetStockGETRequest {

	public static final String  BASE_URL = Constant.AppConstant.BASE_URL;
    public static final String  Add_STOCK_URL = BASE_URL + Constant.APIMethods.GET_STOCK_DETAIL;
    private AGetStockGETRequestCallback callback;

    public interface AGetStockGETRequestCallback {
        public void OnCompleteCallback(JSONObject obj, AjaxStatus status);
    }

    public GetStockGETRequest(Context mContext, String strStockID , AGetStockGETRequestCallback caller) {
        AQuery aq = new AQuery(mContext);
        this.callback = caller;

        aq.ajax(Add_STOCK_URL+"?stock_id="+strStockID, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jObject, AjaxStatus status) {
                try {
                    if(callback != null){
                        System.out.println("jObject Detailsss isss: "+jObject);
                        callback.OnCompleteCallback(jObject,status);
                    }else{
                        callback.OnCompleteCallback(null,null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.method(AQuery.METHOD_GET).header(Constant.AppConstant.AUTHORIZATION, PreferenceClass.getStringPreferences(mContext,Constant.User.AUTHORIZATION_TOKEN)));

    }
}
