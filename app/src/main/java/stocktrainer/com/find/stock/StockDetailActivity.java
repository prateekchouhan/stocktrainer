package stocktrainer.com.find.stock;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import stocktrainer.com.R;
import stocktrainer.com.Utility.ConnectionDetector;
import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.account.server.AccountGETRequest;
import stocktrainer.com.constant.Constant;
import stocktrainer.com.find.stock.server.AddStockPOSTRequest;
import stocktrainer.com.find.stock.server.BuySellStockPOSTRequest;
import stocktrainer.com.find.stock.server.GetStockGETRequest;
import stocktrainer.com.find.stock.server.StockStatsPOSTRequest;

public class StockDetailActivity extends AppCompatActivity implements OnChartGestureListener,
        OnChartValueSelectedListener, View.OnClickListener{
    private Context mContext;
    private Toolbar mToolbar;
    private Dialog dialogBuySell;
    private String strIDStock="";
    private String strStocType="";
    private ConnectionDetector cd;


    //Buy Sell Valriable
    private int buyingStockCapacity=0;
    private int boughtStockCapacity=0;
    private int brokerageFee=0;
    private float totalDebit=0.0f;
    private float stockPrice=0.0f;

    private LineChart mChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_detail);
        mContext=this;

        //set action bar
        setActionBar();

        //init component
        initComponent();

    }

    private void initComponent(){
        cd = new ConnectionDetector(mContext);
        dialogBuySell();

        (findViewById(R.id.layoutStockDataDetail)).setVisibility(View.VISIBLE);
        (findViewById(R.id.btnData)).setSelected(true);
        ((Button)findViewById(R.id.btnData)).setTextColor(Color.WHITE);

        (findViewById(R.id.layoutStockDataDetail)).setVisibility(View.VISIBLE);
        (findViewById(R.id.layoutGraph)).setVisibility(View.GONE);

        //set listner
        setClickListener();

        //set font style
        setFontStyle();

        setSelectorFalse();
    }

private void initLineGraph(String search_data_params,String response){
    mChart = (LineChart) findViewById(R.id.graphLineStock);
    mChart.setOnChartGestureListener(this);
    mChart.setOnChartValueSelectedListener(this);
    mChart.setDrawGridBackground(false);

    // add data

    try{
        JSONObject jObject = new JSONObject(response);

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();
        l.setForm(Legend.LegendForm.LINE);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setScaleXEnabled(true);
        mChart.setScaleYEnabled(true);
        mChart.setDescription("");

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        rightAxis.setAxisMaxValue(Float.valueOf(jObject.getString("max")+1));
        rightAxis.setAxisMinValue(Float.valueOf(jObject.getString("min"))-1);
        rightAxis.setLabelCount(10,true);

        rightAxis.setDrawLimitLinesBehindData(false);


        //X-AXIS
        XAxis xAxis = mChart.getXAxis();
        xAxis.setTextSize(12f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setEnabled(true);
        xAxis.disableGridDashedLine();
        xAxis.setSpaceBetweenLabels(5);
        xAxis.setDrawGridLines(false);
        xAxis.setAvoidFirstLastClipping(true);

        //mChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mChart.getAxisLeft().setEnabled(false);
        mChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
        //  dont forget to refresh the drawing
        mChart.invalidate();


        setData(search_data_params,jObject.getJSONArray("data"));
    }catch (Exception e){
        e.printStackTrace();
    }
}
    private void setClickListener(){
        (findViewById(R.id.btnData)).setOnClickListener(this);
        (findViewById(R.id.btnGraph)).setOnClickListener(this);
        (findViewById(R.id.btnBuy)).setOnClickListener(this);
        (findViewById(R.id.btnSell)).setOnClickListener(this);
        (findViewById(R.id.btnWatch)).setOnClickListener(this);

        //set click for graph options
        (findViewById(R.id.btn1d)).setOnClickListener(this);
        (findViewById(R.id.btn1w)).setOnClickListener(this);
        (findViewById(R.id.btn1m)).setOnClickListener(this);
        (findViewById(R.id.btn3m)).setOnClickListener(this);
        (findViewById(R.id.btn6m)).setOnClickListener(this);
        (findViewById(R.id.btn1y)).setOnClickListener(this);
        (findViewById(R.id.btn2y)).setOnClickListener(this);
        (findViewById(R.id.btn5y)).setOnClickListener(this);
        (findViewById(R.id.btnMax)).setOnClickListener(this);
    }

    private void setSelectorFalse(){
        (findViewById(R.id.btn1d)).setSelected(false);
        (findViewById(R.id.btn1w)).setSelected(false);
        (findViewById(R.id.btn1m)).setSelected(false);
        (findViewById(R.id.btn3m)).setSelected(false);
        (findViewById(R.id.btn6m)).setSelected(false);
        (findViewById(R.id.btn1y)).setSelected(false);
        (findViewById(R.id.btn2y)).setSelected(false);
        (findViewById(R.id.btn5y)).setSelected(false);
        (findViewById(R.id.btnMax)).setSelected(false);

        ((Button)findViewById(R.id.btn1d)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button)findViewById(R.id.btn1w)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button)findViewById(R.id.btn1m)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button)findViewById(R.id.btn3m)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button)findViewById(R.id.btn6m)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button)findViewById(R.id.btn1y)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button)findViewById(R.id.btn2y)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button)findViewById(R.id.btn5y)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button)findViewById(R.id.btnMax)).setTextColor(getResources().getColor(R.color.colorAccent));
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(cd.isConnectingToInternet()){
            getUserAccountDetails();
        }else{
            Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnData:
                (findViewById(R.id.layoutStockDataDetail)).setVisibility(View.VISIBLE);
                (findViewById(R.id.layoutGraph)).setVisibility(View.GONE);
                (findViewById(R.id.btnData)).setSelected(true);
                (findViewById(R.id.btnGraph)).setSelected(false);
                ((Button)findViewById(R.id.btnData)).setTextColor(Color.WHITE);
                ((Button)findViewById(R.id.btnGraph)).setTextColor(Color.BLACK);
                break;
            case R.id.btnGraph:
                (findViewById(R.id.layoutStockDataDetail)).setVisibility(View.GONE);
                (findViewById(R.id.layoutGraph)).setVisibility(View.VISIBLE);
                (findViewById(R.id.btnData)).setSelected(false);
                (findViewById(R.id.btnGraph)).setSelected(true);
                //initLineGraph();
                setSelectorFalse();
                (findViewById(R.id.btnMax)).setSelected(true);
                ((Button)findViewById(R.id.btnMax)).setTextColor(getResources().getColor(R.color.colorWhite));
                ((Button)findViewById(R.id.btnData)).setTextColor(Color.BLACK);
                ((Button)findViewById(R.id.btnGraph)).setTextColor(Color.WHITE);
                break;
            case R.id.btnBuy:
                strStocType="buy";
                setSellDialogData("BUY");
                dialogBuySell.show();
                break;
            case R.id.btnSell:
                strStocType="sell";
                setSellDialogData("SELL");
                dialogBuySell.show();
                break;
            case R.id.btnWatch:
                addStockWatchlist();
                break;
            case R.id.btnBuySell:
                if(boughtStockCapacity>0){
                    if(boughtStockCapacity<=buyingStockCapacity){
                        if(dialogBuySell!=null && dialogBuySell.isShowing()){
                            dialogBuySell.dismiss();
                        }
                        if(cd.isConnectingToInternet()){
                            stockBuySell(strStocType);
                        }else{
                            Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
                        }
                    }else{
                        if(strStocType.equals("buy")){
                            Toast.makeText(mContext,mContext.getString(R.string.stock_quantity_limit),Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(mContext,mContext.getString(R.string.stock_not_sufficient),Toast.LENGTH_LONG).show();
                        }

                    }
                }else{
                    Toast.makeText(mContext,mContext.getString(R.string.stock_quantity_0),Toast.LENGTH_LONG).show();
                }


                break;
            case R.id.imgvwHeaderBack:
                finish();
                break;
            case R.id.imgvwHeaderCamera:
                View captureView=findViewById(R.id.layoutParent);
                Utils.takeScreenshot(StockDetailActivity.this,captureView);
                break;
            case R.id.btn1d:
                getStockStats(Constant.GraphSearch.GS_1_D);
                setSelectorFalse();
                (findViewById(R.id.btn1d)).setSelected(true);
                ((Button)findViewById(R.id.btn1d)).setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.btn1w:
                getStockStats(Constant.GraphSearch.GS_1_W);

                setSelectorFalse();
                (findViewById(R.id.btn1w)).setSelected(true);
                ((Button)findViewById(R.id.btn1w)).setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.btn1m:
                getStockStats(Constant.GraphSearch.GS_1_M);
                setSelectorFalse();
                (findViewById(R.id.btn1m)).setSelected(true);
                ((Button)findViewById(R.id.btn1m)).setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.btn3m:
                getStockStats(Constant.GraphSearch.GS_3_M);
                setSelectorFalse();
                (findViewById(R.id.btn3m)).setSelected(true);
                ((Button)findViewById(R.id.btn3m)).setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.btn6m:
                getStockStats(Constant.GraphSearch.GS_6_M);
                setSelectorFalse();
                (findViewById(R.id.btn6m)).setSelected(true);
                ((Button)findViewById(R.id.btn6m)).setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.btn1y:
                getStockStats(Constant.GraphSearch.GS_1_Y);
                setSelectorFalse();
                (findViewById(R.id.btn1y)).setSelected(true);
                ((Button)findViewById(R.id.btn1y)).setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.btn2y:
                getStockStats(Constant.GraphSearch.GS_2_Y);
                setSelectorFalse();
                (findViewById(R.id.btn2y)).setSelected(true);
                ((Button)findViewById(R.id.btn2y)).setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.btn5y:
                getStockStats(Constant.GraphSearch.GS_5_Y);
                setSelectorFalse();
                (findViewById(R.id.btn5y)).setSelected(true);
                ((Button)findViewById(R.id.btn5y)).setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.btnMax:
                getStockStats(Constant.GraphSearch.GS_MAX);
                setSelectorFalse();
                (findViewById(R.id.btnMax)).setSelected(true);
                ((Button)findViewById(R.id.btnMax)).setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            default:
                break;
        }
    }

    private void setActionBar(){
        mToolbar= (Toolbar) findViewById(R.id.toolbarStockDetail);
        setSupportActionBar(mToolbar);

        ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setText(mContext.getString(R.string.app_name));
        (mToolbar.findViewById(R.id.imgvwHeaderBack)).setOnClickListener(this);
        (mToolbar.findViewById(R.id.imgvwHeaderCamera)).setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }


    private void setFontStyle(){
        ((TextView)findViewById(R.id.txtNameStockDetail)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)findViewById(R.id.txtValueStockDetail)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)findViewById(R.id.txtTypeStockDetail)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));

        ((Button)findViewById(R.id.btnData)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((Button)findViewById(R.id.btnGraph)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((Button)findViewById(R.id.btnBuy)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((Button)findViewById(R.id.btnSell)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((Button)findViewById(R.id.btnWatch)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));

        ((TextView)findViewById(R.id.txtHighPrice)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtOpenPrice)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtChange)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtAskPrice)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtAskSize)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txt52WeekHigh)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txt1YrReturn)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtPERatio)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtMessageStockDetail)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));


        ((TextView)findViewById(R.id.txtSetHighPrice)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtSetOpenPrice)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtSetChange)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtSetAskPrice)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtSetAskSize)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtSet52WeekHigh)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtSet1YrReturn)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)findViewById(R.id.txtSetPERatio)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));


       /* Dialog Font Style*/

        ((TextView)dialogBuySell.findViewById(R.id.txtPlaceOrder)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)dialogBuySell.findViewById(R.id.txtNameStockDetailDialog)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)dialogBuySell.findViewById(R.id.txtCodeStockDetailDialog)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)dialogBuySell.findViewById(R.id.txtValueStockDetailDialog)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));

        ((TextView)dialogBuySell.findViewById(R.id.txtAvailableFund)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)dialogBuySell.findViewById(R.id.txtBuyingCapacity)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)dialogBuySell.findViewById(R.id.txtBuyQuantity)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)dialogBuySell.findViewById(R.id.txtBrokerageFee)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)dialogBuySell.findViewById(R.id.txtTotalDebit)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)dialogBuySell.findViewById(R.id.txtDisclaimer1)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((Button)dialogBuySell.findViewById(R.id.btnBuySell)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

        ((TextView)dialogBuySell.findViewById(R.id.txtSetAvailableFund)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)dialogBuySell.findViewById(R.id.txtSetBuyingCapacity)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)dialogBuySell.findViewById(R.id.edtSetBuyQuantity)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)dialogBuySell.findViewById(R.id.txtSetBrokerageFee)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)dialogBuySell.findViewById(R.id.txtSetTotalDebit)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));


        //set stock details data
        setDataStockDetails();
    }

    private void setDataStockDetails(){

        try{
            String strStockDetail = getIntent().getExtras().getString("FindStockSelected");
            JSONObject jObjectStockDetail = new JSONObject(strStockDetail);
            strIDStock = jObjectStockDetail.getString("id");

            if(cd.isConnectingToInternet()){
                getStockDetail();
                getStockStats(Constant.GraphSearch.GS_MAX);
            }else{
                Toast.makeText(mContext,mContext.getString(R.string.check_network),Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void dialogBuySell() {
        dialogBuySell = new Dialog(mContext);
        dialogBuySell.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogBuySell.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogBuySell.setContentView(R.layout.dialog_stock_buy_sell);

        (dialogBuySell.findViewById(R.id.btnBuySell)).setOnClickListener(this);

    }

    private void setSellDialogData(String request){

        try{
            final JSONObject jObjectStock = new JSONObject(PreferenceClass.getStringPreferences(mContext,"StockDetail"));
            ((TextView)dialogBuySell.findViewById(R.id.txtNameStockDetailDialog)).setText(jObjectStock.getString("name"));
            ((TextView)dialogBuySell.findViewById(R.id.txtCodeStockDetailDialog)).setText(jObjectStock.getString("dataset_code"));
            ((TextView)dialogBuySell.findViewById(R.id.txtValueStockDetailDialog)).setText("  "+mContext.getString(R.string.Rs)+jObjectStock.getString("current_price"));

            if(request.equals("BUY")){
                ((TextView)dialogBuySell.findViewById(R.id.txtBuyingCapacity)).setText(mContext.getString(R.string.Buying_Capacity));
                ((TextView)dialogBuySell.findViewById(R.id.txtBuyQuantity)).setText(mContext.getString(R.string.Buying_Quantity));
                ((TextView)dialogBuySell.findViewById(R.id.txtTotalDebit)).setText(mContext.getString(R.string.Total_Debit));
                ((Button)dialogBuySell.findViewById(R.id.btnBuySell)).setText(mContext.getString(R.string.Buy));
            }else{
                ((TextView)dialogBuySell.findViewById(R.id.txtBuyingCapacity)).setText(mContext.getString(R.string.Share_Available));
                ((TextView)dialogBuySell.findViewById(R.id.txtBuyQuantity)).setText(mContext.getString(R.string.Sell_Quantity));
                ((TextView)dialogBuySell.findViewById(R.id.txtTotalDebit)).setText(mContext.getString(R.string.Proceeds));
                ((Button)dialogBuySell.findViewById(R.id.btnBuySell)).setText(mContext.getString(R.string.Sell));
            }

            brokerageFee = 15;
            stockPrice =Float.parseFloat(jObjectStock.getString("current_price"));
            double val = (Double.parseDouble(PreferenceClass.getStringPreferences(mContext,Constant.User.AVAILABLE_FUND)) - brokerageFee)/(Float.parseFloat(jObjectStock.getString("current_price")));
            buyingStockCapacity = (int) val;


            ((TextView)dialogBuySell.findViewById(R.id.txtSetAvailableFund)).setText(""+PreferenceClass.getStringPreferences(mContext,Constant.User.AVAILABLE_FUND));
            ((TextView)dialogBuySell.findViewById(R.id.txtSetBrokerageFee)).setText(""+brokerageFee);
            ((TextView)dialogBuySell.findViewById(R.id.txtSetBuyingCapacity)).setText(""+buyingStockCapacity);


            ((EditText)dialogBuySell.findViewById(R.id.edtSetBuyQuantity)).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    String strText = charSequence.toString().trim();
                    try {
                        if(strText.length()>0){
                            boughtStockCapacity = Integer.parseInt(strText);

                            totalDebit = (Float.parseFloat(jObjectStock.getString("current_price")))*boughtStockCapacity+brokerageFee;
                            ((TextView)dialogBuySell.findViewById(R.id.txtSetTotalDebit)).setText(mContext.getString(R.string.Rs)+" "+totalDebit);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }



    }

    public void addStockWatchlist() {

        Map<String, Object> params = new HashMap<>();
        params.put("stock_id", strIDStock);

        new AddStockPOSTRequest(mContext,params,new AddStockPOSTRequest.AddStockPOSTRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj,AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if (status.getCode() == 200) {
                            try{
                                if(obj.getString("status").equals("true")){
                                    Toast.makeText(mContext,mContext.getString(R.string.Stock_added),Toast.LENGTH_LONG).show();
                                    finish();
                                }else{
                                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }

    public void getStockStats(final String search_data_params) {

        Map<String, Object> params = new HashMap<>();
        params.put("range", search_data_params);
        params.put("stock_id", strIDStock);

        new StockStatsPOSTRequest(mContext,params,new StockStatsPOSTRequest.StockStatsPOSTRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj,AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if (status.getCode() == 200) {
                            try{
                                System.out.println("Graph Statics:  "+obj);
                                if(obj.getString("status").equals("true")){
                                    initLineGraph(search_data_params,obj.toString());
                                }else{
                                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }

    public void stockBuySell(final String buy_sell_type) {


        Map<String, Object> params = new HashMap<>();
        params.put("stock_id", strIDStock);
        params.put("type", buy_sell_type);
        params.put("quantity", boughtStockCapacity);
        params.put("price", stockPrice);


        new BuySellStockPOSTRequest(mContext,params,new BuySellStockPOSTRequest.BuySellStockPOSTRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj,AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if (status.getCode() == 200) {
                            try{
                                System.out.println("stockBuySell Response issss: "+obj);
                                if(obj.getString("status").equals("true")){
                                    if(buy_sell_type.equals("buy")){
                                        Toast.makeText(mContext,"Stock is bought.",Toast.LENGTH_LONG).show();
                                    }else{
                                        Toast.makeText(mContext,"Stock is sold.",Toast.LENGTH_LONG).show();
                                    }
                                    finish();
                                }else{
                                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }

    public void getStockDetail() {

        new GetStockGETRequest(mContext,strIDStock,new GetStockGETRequest.AGetStockGETRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj,AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if (status.getCode() == 200) {
                            try{
                                PreferenceClass.setStringPreference(mContext,"StockDetail",obj.toString());

                                ((TextView)findViewById(R.id.txtNameStockDetail)).setText(obj.getString("name")+" ("+obj.getString("dataset_code")+")");
                                ((TextView)findViewById(R.id.txtTypeStockDetail)).setText(obj.getString("database_code"));
                                ((TextView)findViewById(R.id.txtValueStockDetail)).setText(mContext.getString(R.string.Rs)+obj.getString("current_price"));

                                ((TextView)findViewById(R.id.txtSetHighPrice)).setText(obj.getJSONObject("exchange_rate").getString("high_price"));
                                ((TextView)findViewById(R.id.txtSetOpenPrice)).setText(obj.getJSONObject("exchange_rate").getString("open_price"));
                                ((TextView)findViewById(R.id.txtSetChange)).setText(obj.getString("diff_percent"));
                                ((TextView)findViewById(R.id.txtSet52WeekHigh)).setText(obj.getJSONObject("stock_52_week").getString("high"));

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else if (status.getCode() == 401) {
                            Toast.makeText(mContext,mContext.getString(R.string.Invalide_Credential),Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }

    public void getUserAccountDetails() {
        new AccountGETRequest(mContext,new AccountGETRequest.AccountGETRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if (status.getCode() == 200) {
                            if(obj.getString("status").equals("true")){
                                JSONObject jObj=obj.getJSONObject("data");
                                PreferenceClass.setStringPreference(mContext, Constant.User.AVAILABLE_FUND,""+jObj.getString("available_cash"));
                            }else{
                                Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                            }
                        }else {
                            Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }


    private void setData(String filter,JSONArray jsonArrayData) {
        ArrayList<String> xVals = new ArrayList<>();

        ArrayList<Entry> yVals = new ArrayList<>();

        yVals.clear();
        xVals.clear();
        try{
            if(jsonArrayData!=null && jsonArrayData.length()>0){
                for (int i = 0; i < jsonArrayData.length(); i++) {
                    float val = (Float.valueOf(jsonArrayData.getJSONObject(i).getString("amount")));
                    yVals.add(new Entry(val, i));
                    String spliteDate[] = jsonArrayData.getJSONObject(i).getString("date").split("-");
                    if(filter.equals(Constant.GraphSearch.GS_MAX) || filter.equals(Constant.GraphSearch.GS_5_Y) || filter.equals(Constant.GraphSearch.GS_2_Y) ){
                        xVals.add(""+spliteDate[1]+"/"+spliteDate[0].substring(2,4));
                    }else{
                        xVals.add(""+spliteDate[1]+"/"+spliteDate[2]);
                    }

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        LineDataSet set1;

        // create a dataset and give it a type
        set1 = new LineDataSet(yVals, "");

        set1.setFillAlpha(110);
        set1.setLineWidth(0f);
        set1.setCircleRadius(0f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(0f);
        set1.setDrawFilled(true);
        set1.setFillColor(Color.parseColor("#1F8CE1"));
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(xVals, dataSets);

        // set data
        mChart.setData(data);

    }


    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if(lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            mChart.highlightValues(null); // or highlightTouch(null) for callback to onNothingSelected(...)
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("LOWHIGH", "low: " + mChart.getLowestVisibleXIndex() + ", high: " + mChart.getHighestVisibleXIndex());
        Log.i("MIN MAX", "xmin: " + mChart.getXChartMin() + ", xmax: " + mChart.getXChartMax() + ", ymin: " + mChart.getYChartMin() + ", ymax: " + mChart.getYChartMax());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }
}
