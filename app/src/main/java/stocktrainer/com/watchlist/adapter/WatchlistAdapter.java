package stocktrainer.com.watchlist.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

import stocktrainer.com.R;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.find.stock.listener.RecyclerClickListner;


public class WatchlistAdapter extends RecyclerView.Adapter<WatchlistAdapter.Watchlist> {
    ArrayList<JSONObject> lstWatchlist;
    Context mContext;
    RecyclerClickListner mRecyclerClickListner;

    public WatchlistAdapter(Context context, ArrayList<JSONObject> lstWatchlist, RecyclerClickListner mRecyclerClickListner) {
        this.mContext = context;
        this.lstWatchlist = lstWatchlist;
        this.mRecyclerClickListner = mRecyclerClickListner;
    }

    @Override
    public Watchlist onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_watchlist_item, parent, false);
        Watchlist imh = new Watchlist(v);
        return imh;
    }

    @Override
    public void onBindViewHolder(final Watchlist viewHolder, final int position) {
        try {

            viewHolder.itemViewSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mRecyclerClickListner.onItemClick(viewHolder.itemViewSelected,position);
                }
            });

            //Set font style
            setFontStyle(viewHolder);

            viewHolder.txtSetTicker.setText(lstWatchlist.get(position).getString("dataset_code"));
            viewHolder.txtSetEquityName.setText(lstWatchlist.get(position).getString("name"));
            viewHolder.txtSetPrice.setText(mContext.getString(R.string.Rs)+lstWatchlist.get(position).getString("price"));
            viewHolder.txtSetNumUnit.setText(lstWatchlist.get(position).getString("price_diff"));
            viewHolder.txtSetValuePercentage.setText(lstWatchlist.get(position).getString("percent_diff")+"%");

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (lstWatchlist != null) {
            return lstWatchlist.size();
        }
        return 0;
    }

    private void setFontStyle(Watchlist viewHolder){
        viewHolder.txtSetTicker.setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        viewHolder.txtSetPrice.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));

        viewHolder.txtSetEquityName.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        viewHolder.txtSetNumUnit.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        viewHolder.txtSetValuePercentage.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
    }

    public static class Watchlist extends RecyclerView.ViewHolder {
        TextView txtSetTicker,txtSetPrice,txtSetEquityName,txtSetNumUnit,txtSetValuePercentage;
        private View itemViewSelected;
        Watchlist(View itemView) {
            super(itemView);
            itemViewSelected=itemView;
            txtSetTicker = itemView.findViewById(R.id.txtSetTicker);
            txtSetPrice = itemView.findViewById(R.id.txtSetPrice);
            txtSetEquityName = itemView.findViewById(R.id.txtSetEquityName);
            txtSetNumUnit = itemView.findViewById(R.id.txtSetNumUnit);
            txtSetValuePercentage = itemView.findViewById(R.id.txtSetValuePercentage);
        }
    }
}