package stocktrainer.com.watchlist.server;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.Map;

import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.constant.Constant;

public class WatchlistStockGETRequest {

	public static final String  BASE_URL = Constant.AppConstant.BASE_URL;
    public static final String  GET_WATCHLIST_URL = BASE_URL + Constant.APIMethods.GET_WATCHLIST;
    private WatchlistStockGETRequestCallback callback;

    public interface WatchlistStockGETRequestCallback {
        public void OnCompleteCallback(JSONObject obj, AjaxStatus status);
    }

    public WatchlistStockGETRequest(Context mContext, WatchlistStockGETRequestCallback caller) {
        AQuery aq = new AQuery(mContext);
        this.callback = caller;

        aq.ajax(GET_WATCHLIST_URL, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jObject, AjaxStatus status) {
                try {
                    if(callback != null){
                        callback.OnCompleteCallback(jObject,status);
                    }else{
                        callback.OnCompleteCallback(null,null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.method(AQuery.METHOD_GET).header(Constant.AppConstant.AUTHORIZATION, PreferenceClass.getStringPreferences(mContext,Constant.User.AUTHORIZATION_TOKEN)));

    }
}
