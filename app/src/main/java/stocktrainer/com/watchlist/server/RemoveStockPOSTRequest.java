package stocktrainer.com.watchlist.server;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.Map;

import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.constant.Constant;

public class RemoveStockPOSTRequest {

	public static final String  BASE_URL = Constant.AppConstant.BASE_URL;
    public static final String  REMOVE_STOCK_URL = BASE_URL + Constant.APIMethods.REMOVE_STOCK_WATCHLIST;
    private RemoveStockPOSTRequestCallback callback;

    public interface RemoveStockPOSTRequestCallback {
        public void OnCompleteCallback(JSONObject obj, AjaxStatus status);
    }

    public RemoveStockPOSTRequest(Context mContext, Map<String, Object> params , RemoveStockPOSTRequestCallback caller) {
        AQuery aq = new AQuery(mContext);
        this.callback = caller;

        aq.ajax(REMOVE_STOCK_URL, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jObject, AjaxStatus status) {
                try {
                    if(callback != null){
                        callback.OnCompleteCallback(jObject,status);
                    }else{
                        callback.OnCompleteCallback(null,null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.method(AQuery.METHOD_POST).header(Constant.AppConstant.AUTHORIZATION, PreferenceClass.getStringPreferences(mContext,Constant.User.AUTHORIZATION_TOKEN)));

    }
}
