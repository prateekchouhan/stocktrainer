package stocktrainer.com.constant;

public class Constant {

    public static class AppConstant {
        public static String BASE_URL = "http://api.dum-e.com/";
        public static String AUTHORIZATION = "Authorization";
        public static String ANDROID = "android";
        public static String FCM_TOKEN = "fcm_token";
    }
    public static class APIMethods{
        //API's function name
        public static String LOGIN = "authentication/login";
        public static String SIGNUP = "authentication/signup";
        public static String FORGOT_PASSWORD = "authentication/forget_password";
        public static String VERIFY_OTP = "authentication/login_process";
        public static String FIND_STOCK = "api/find_stock";
        public static String GET_STOCK_DETAIL = "api/stock_details";
        public static String Add_STOCK_WATCHLIST = "api/set_watchlist";
        public static String REMOVE_STOCK_WATCHLIST = "api/remove_watchlist";
        public static String GET_WATCHLIST = "api/user_watchlist";
        public static String GET_PORTFOLIO = "api/user_portfolio";
        public static String GET_TRANSACTION = "api/user_transactions";
        public static String USER_ACCOUNT = "api/account_detail";
        public static String USER_TRADE = "api/user_trade";
        public static String STOCK_STATS = "api/stock_stats";
        public static String TOP_LOSER = "api/top_losers";
        public static String TOP_GAINER = "api/top_gainers";
        public static String SAVE_TOKEN = "api/save_user_token";
        public static String GET_CHALLENGE = "api/user_challenge?period=";
        public static String UPDATE_PROFILE = "api/update_user_details";
        public static String CHANGE_PASSWORD = "api/update_password";
    }

    public static class User {
        public static String IS_LOGIN = "is_login";
        public static String MOBILE_NUMBER = "mobile_number";
        public static String MOBILE_OTP = "mobile_otp";
        public static String USER_ID = "user_id";
        public static String USER_NAME = "user_name";
        public static String USER_EMAIL = "user_email";
        public static String AUTHORIZATION_TOKEN = "authorization_token";
        public static String AVAILABLE_FUND = "available_fund";
    }

    public static class GraphSearch {
        public static String GS_1_D = "7_days";
        public static String GS_1_W = "7_days";
        public static String GS_1_M = "30_days";
        public static String GS_3_M = "3_months";
        public static String GS_6_M = "6_months";
        public static String GS_1_Y = "1_year";
        public static String GS_2_Y = "2_year";
        public static String GS_5_Y = "5_year";
        public static String GS_MAX = "max";
    }


}
