package stocktrainer.com.feedback;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import stocktrainer.com.R;
import stocktrainer.com.Utility.Utils;

/**
 * Created by mac on 21/09/17.
 */

public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mContext;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        mContext=this;

        //set action bar
        setActionBar();

        //set font style
        setFontStyle();

        //initialise component
        initComponent();
    }

    private void initComponent(){
        (findViewById(R.id.btnSubmit)).setOnClickListener(this);
        (findViewById(R.id.txtFeedbackContent2)).setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgvwHeaderBack:
                finish();
                break;
            case R.id.btnSubmit:
                Toast.makeText(mContext,"In Process...",Toast.LENGTH_LONG).show();
                break;
            case R.id.txtFeedbackContent2:
                Intent mailClient = new Intent(Intent.ACTION_VIEW);
                mailClient.setClassName("com.google.android.gm", "com.google.android.gm.ConversationListActivity");
                mailClient.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"support@yourdomain.com"});
                mailClient.putExtra(android.content.Intent.EXTRA_SUBJECT, "Strock Trainer Feedback");
                mailClient.putExtra(android.content.Intent.EXTRA_TEXT, "");
                startActivity(mailClient);
                break;
            default:
                break;
        }
    }

    private void setFontStyle() {
        ((TextView) findViewById(R.id.txtFeedbackContent1)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) findViewById(R.id.txtFeedbackContent2)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((EditText) findViewById(R.id.edtFeedback)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((Button) findViewById(R.id.btnSubmit)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
    }

    private void setActionBar(){
        mToolbar= (Toolbar) findViewById(R.id.toolbarFeedback);
        setSupportActionBar(mToolbar);

        ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setText(mContext.getString(R.string.Feedback));
        (mToolbar.findViewById(R.id.imgvwHeaderBack)).setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }
}
