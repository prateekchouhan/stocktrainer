package stocktrainer.com.magazine.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import stocktrainer.com.R;
import stocktrainer.com.portfolio.fragment.PortfolioFragment;

/**
 * Created by mac on 11/09/17.
 */

public class MagazineFragment extends Fragment {
    private Context mContext;
    private View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView =  inflater.inflate(R.layout.fragment_magazine, container, false);
        mContext = getActivity();

        return rootView;
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);

        (activity.findViewById(R.id.imgvwHeaderRefresh)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Refreshing...",Toast.LENGTH_LONG).show();
            }
        });
    }
}
