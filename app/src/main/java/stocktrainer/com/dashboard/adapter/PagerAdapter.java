package stocktrainer.com.dashboard.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import stocktrainer.com.challenge.fragment.ChallengeFragment;
import stocktrainer.com.find.stock.fragment.FindStockFragment;
import stocktrainer.com.magazine.fragment.MagazineFragment;
import stocktrainer.com.account.fragment.MyAccountFragment;
import stocktrainer.com.portfolio.fragment.PortfolioFragment;
import stocktrainer.com.top.movers.fragment.TopMoverFragment;
import stocktrainer.com.transaction.fragment.TransactionsFragment;
import stocktrainer.com.watchlist.fragment.WatchlistFragment;

/**
 * Created by mac on 11/09/17.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                PortfolioFragment fragment_portfolio = new PortfolioFragment();
                return fragment_portfolio;
            case 1:
                WatchlistFragment fragment_watchlist = new WatchlistFragment();
                return fragment_watchlist;
            case 2:
                MagazineFragment fragment_magazine = new MagazineFragment();
                return fragment_magazine;
            case 3:
                ChallengeFragment fragment_challenge = new ChallengeFragment();
                return fragment_challenge;
            case 4:
                FindStockFragment fragment_find_stock = new FindStockFragment();
                return fragment_find_stock;
            case 5:
                TopMoverFragment fragment_top_movers = new TopMoverFragment();
                return fragment_top_movers;
            case 6:
                MyAccountFragment fragment_my_account = new MyAccountFragment();
                return fragment_my_account;
            case 7:
                TransactionsFragment fragment_transactions = new TransactionsFragment();
                return fragment_transactions;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}