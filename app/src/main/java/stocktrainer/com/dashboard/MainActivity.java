package stocktrainer.com.dashboard;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import stocktrainer.com.R;
import stocktrainer.com.Utility.DevicePermission;
import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.auth.LoginActivity;
import stocktrainer.com.constant.Constant;
import stocktrainer.com.dashboard.adapter.CubeOutTransformer;
import stocktrainer.com.dashboard.adapter.PagerAdapter;
import stocktrainer.com.dashboard.server.TokenPOSTRequest;
import stocktrainer.com.feedback.FeedbackActivity;
import stocktrainer.com.find.stock.server.BuySellStockPOSTRequest;
import stocktrainer.com.settings.SettingActivity;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Context mContext;
    Toolbar mToolbar;
    private ViewPager viewPager;
    private DrawerLayout drawer;
    private Dialog dialogLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext =this;

        PreferenceClass.setBooleanPreference(mContext, Constant.User.IS_LOGIN,true);

        //set action bar
        setActionBar();

        //set dialog logout
        dialogLogout();

        //set tab with viewpager
        setTabBarPager();

        //save token for notification
        //saveToken();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, mToolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        //set nav header with drawer
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.inflateHeaderView(R.layout.view_nav_header);
        TextView txtNameNavHeader = header.findViewById(R.id.txtNameNavHeader);
        TextView txtEmailNavHeader = header.findViewById(R.id.txtEmailNavHeader);

        //set font style for nav header text
        txtNameNavHeader.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        txtEmailNavHeader.setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));

        //set text for nav header text
        txtNameNavHeader.setText(PreferenceClass.getStringPreferences(mContext,Constant.User.USER_NAME));
        txtEmailNavHeader.setText(PreferenceClass.getStringPreferences(mContext,Constant.User.MOBILE_NUMBER));


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.menuPortfolio) {
                    setToolVisibility(1);
                    viewPager.setCurrentItem(0);
                } else if (id == R.id.menuWatchlist) {
                    setToolVisibility(1);
                    viewPager.setCurrentItem(1);
                } else if (id == R.id.menuMagazine) {
                    setToolVisibility(1);
                    viewPager.setCurrentItem(2);
                } else if (id == R.id.menuChallenge) {
                    setToolVisibility(1);
                    viewPager.setCurrentItem(3);
                }else if (id == R.id.menuFindStocks) {
                    setToolVisibility(0);
                    viewPager.setCurrentItem(4);
                }else if (id == R.id.menuTopMovers) {
                    setToolVisibility(1);
                    viewPager.setCurrentItem(5);
                }else if (id == R.id.menuMyAccount) {
                    setToolVisibility(1);
                    viewPager.setCurrentItem(6);
                }else if (id == R.id.menuTransactions) {
                    setToolVisibility(1);
                    viewPager.setCurrentItem(7);
                }else if (id == R.id.menuSettings) {
                    startActivity(new Intent(mContext, SettingActivity.class));
                }else if (id == R.id.menuHelpFeedback) {
                    startActivity(new Intent(mContext, FeedbackActivity.class));
                }else if (id == R.id.menuLogout) {
                    if(dialogLogout!=null){
                        dialogLogout.show();
                    }
                }

                drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                assert drawer != null;
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    private void setTabBarPager(){
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.Portfolio));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Watchlist));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Magazine));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Challenge));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Find_Stocks));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Top_Movers));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.My_Account));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Transactions));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

       viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setPageTransformer(true, new CubeOutTransformer());
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==4){
                    setToolVisibility(0);
                }else{
                    setToolVisibility(1);
                }
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void dialogLogout() {
        dialogLogout = new Dialog(mContext);
        dialogLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLogout.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogLogout.setContentView(R.layout.dialog_logout);
        //dialogBuySell.setCancelable(false);

        (dialogLogout.findViewById(R.id.btnYes)).setOnClickListener(this);
        (dialogLogout.findViewById(R.id.btnNo)).setOnClickListener(this);

        ((TextView)dialogLogout.findViewById(R.id.txtLogout)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView)dialogLogout.findViewById(R.id.txtLogoutContent)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((Button)dialogLogout.findViewById(R.id.btnYes)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((Button)dialogLogout.findViewById(R.id.btnNo)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));


    }

    private void setActionBar(){
        mToolbar= (Toolbar) findViewById(R.id.toolbarMain);
        setSupportActionBar(mToolbar);
        (mToolbar.findViewById(R.id.imgvwHeaderCamera)).setOnClickListener(this);
        ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setText(R.string.app_name);

    }

    private void setToolVisibility(int visibility){
        (mToolbar.findViewById(R.id.edtSearch)).setVisibility(View.GONE);
        if(visibility==1){
            (mToolbar.findViewById(R.id.layoutToolbar)).setVisibility(View.VISIBLE);
            (mToolbar.findViewById(R.id.layoutToolbar11)).setVisibility(View.GONE);
        }else{
            (mToolbar.findViewById(R.id.layoutToolbar)).setVisibility(View.GONE);
            (mToolbar.findViewById(R.id.layoutToolbar11)).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgvwHeaderCamera:
                View captureView=findViewById(R.id.layoutParent);
                Utils.takeScreenshot(MainActivity.this,captureView);
                break;
            case R.id.btnYes:
                if(dialogLogout.isShowing() && dialogLogout!=null){
                    dialogLogout.dismiss();
                }
                PreferenceClass.clearPreference(mContext);
                startActivity(new Intent(mContext, LoginActivity.class));
                finish();
                break;
            case R.id.btnNo:
                if(dialogLogout.isShowing() && dialogLogout!=null){
                    dialogLogout.dismiss();
                }
                break;
            default:
                break;
        }
    }

    public void saveToken() {

        System.out.println("AUTHORIZATION_TOKEN iss: "+PreferenceClass.getStringPreferences(mContext,Constant.User.AUTHORIZATION_TOKEN));
        Map<String, Object> params = new HashMap<>();
        params.put("device_type", Constant.AppConstant.ANDROID);
        params.put("device_token", PreferenceClass.getStringPreferences(mContext,Constant.AppConstant.FCM_TOKEN));

        new TokenPOSTRequest(mContext,params,new TokenPOSTRequest.TokenPOSTRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        System.out.println("DeviceToken isss: "+obj);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        } );
    }
}
