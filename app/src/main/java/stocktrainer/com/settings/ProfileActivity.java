package stocktrainer.com.settings;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import stocktrainer.com.R;
import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.constant.Constant;
import stocktrainer.com.find.stock.server.AddStockPOSTRequest;
import stocktrainer.com.settings.server.UpdateProfilePOSTRequest;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mContext;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mContext=this;

        //set action bar
        setActionBar();

        //set font style
        setFontStyle();


        //initialise component
        initComponent();

    }

    private void initComponent(){
        ((EditText) findViewById(R.id.edtName)).setText(PreferenceClass.getStringPreferences(mContext, Constant.User.USER_NAME));
        ((EditText) findViewById(R.id.edtEmail)).setText(PreferenceClass.getStringPreferences(mContext, Constant.User.USER_EMAIL));
        ((EditText) findViewById(R.id.edtMobile)).setText(PreferenceClass.getStringPreferences(mContext, Constant.User.MOBILE_NUMBER));
        (findViewById(R.id.txtChangePassword)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgvwHeaderBack:
                finish();
                break;
            case R.id.txtChangePassword:
                startActivity(new Intent(mContext,ChangePasswordActivity.class));
                break;
            case R.id.txtEditUpdate:
                String strText = ((TextView)findViewById(R.id.txtEditUpdate)).getText().toString().trim();
                String strName = ((EditText)findViewById(R.id.edtName)).getText().toString().trim();
                String strEmail = ((EditText)findViewById(R.id.edtEmail)).getText().toString().trim();
                if(strText.equals(mContext.getString(R.string.Edit))){
                    findViewById(R.id.edtName).setEnabled(true);
                    findViewById(R.id.edtEmail).setEnabled(true);
                    findViewById(R.id.edtName).setClickable(true);
                    findViewById(R.id.edtEmail).setClickable(true);
                    ((TextView)findViewById(R.id.txtEditUpdate)).setText(mContext.getString(R.string.Update));
                }else{
                    if(strName.trim().length()==0){
                        Toast.makeText(mContext,mContext.getString(R.string.name_blank),Toast.LENGTH_LONG).show();
                    }else if(strEmail.trim().length()==0){
                        Toast.makeText(mContext,mContext.getString(R.string.email_blank),Toast.LENGTH_LONG).show();
                    }else if(!Utils.isValidEmail(strEmail)){
                        Toast.makeText(mContext,mContext.getString(R.string.email_invalid),Toast.LENGTH_LONG).show();
                    }else{
                        updateProfile(strName,strEmail);
                    }
                }

                break;
            default:
                break;
        }
    }

    private void setFontStyle() {
        ((TextView) findViewById(R.id.txtChangePassword)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((EditText) findViewById(R.id.edtName)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((EditText) findViewById(R.id.edtEmail)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((EditText) findViewById(R.id.edtMobile)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) findViewById(R.id.txtEditUpdate)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));

    }

    private void setActionBar(){
        mToolbar= (Toolbar) findViewById(R.id.toolbarProfile);
        setSupportActionBar(mToolbar);

        ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setText(mContext.getString(R.string.Profile));
        (mToolbar.findViewById(R.id.imgvwHeaderBack)).setOnClickListener(this);
        (mToolbar.findViewById(R.id.txtEditUpdate)).setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    public void updateProfile(final String name,final String email) {

        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("email", email);

        new UpdateProfilePOSTRequest(mContext,params,new UpdateProfilePOSTRequest.UpdateProfilePOSTRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if (status.getCode() == 200) {
                            try{
                                System.out.println("Profile Update Object issss: "+obj);
                                if(obj.getString("status").equals("true")){
                                    Toast.makeText(mContext,mContext.getString(R.string.Profile_Updated),Toast.LENGTH_LONG).show();
                                    PreferenceClass.setStringPreference(mContext,Constant.User.USER_NAME,name);
                                    PreferenceClass.setStringPreference(mContext,Constant.User.USER_EMAIL,email);
                                    findViewById(R.id.edtName).setEnabled(false);
                                    findViewById(R.id.edtEmail).setEnabled(false);
                                    findViewById(R.id.edtName).setClickable(false);
                                    findViewById(R.id.edtEmail).setClickable(false);
                                    ((TextView)findViewById(R.id.txtEditUpdate)).setText(mContext.getString(R.string.Edit));
                                }else{
                                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }
}
