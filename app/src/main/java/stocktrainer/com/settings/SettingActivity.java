package stocktrainer.com.settings;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import stocktrainer.com.R;
import stocktrainer.com.Utility.Utils;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mContext;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        mContext=this;

        //set action bar
        setActionBar();

        //set font style
        setFontStyle();


        //initialise component
        initComponent();

    }

    private void initComponent(){
        (findViewById(R.id.txtNotificationSettings)).setOnClickListener(this);
        (findViewById(R.id.txtPrivacyPolicy)).setOnClickListener(this);
        (findViewById(R.id.txtSelectLanguage)).setOnClickListener(this);
        (findViewById(R.id.txtEditProfile)).setOnClickListener(this);
        (findViewById(R.id.txtAppPermissions)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgvwHeaderBack:
                finish();
                break;
            case R.id.txtNotificationSettings:
                    startActivity(new Intent(mContext,SettingOptionActivity.class).putExtra("SettingOption","ForNotification"));
                break;
            case R.id.txtPrivacyPolicy:
                startActivity(new Intent(mContext,SettingOptionActivity.class).putExtra("SettingOption","ForPrivacyPolicy"));
                break;
            case R.id.txtSelectLanguage:
                startActivity(new Intent(mContext,SettingOptionActivity.class).putExtra("SettingOption","ForLanguage"));
                break;
            case R.id.txtEditProfile:
                startActivity(new Intent(mContext,ProfileActivity.class));
                break;
            case R.id.txtAppPermissions:
                    Toast.makeText(mContext,"In Process...",Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }

    private void setFontStyle() {
        ((TextView) findViewById(R.id.txtEditProfile)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) findViewById(R.id.txtPrivacyPolicy)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) findViewById(R.id.txtNotificationSettings)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) findViewById(R.id.txtSelectLanguage)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) findViewById(R.id.txtAppPermissions)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
    }

    private void setActionBar(){
        mToolbar= (Toolbar) findViewById(R.id.toolbarSetting);
        setSupportActionBar(mToolbar);

        ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setText(mContext.getString(R.string.Settings));
        (mToolbar.findViewById(R.id.imgvwHeaderBack)).setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }
}
