package stocktrainer.com.settings;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import stocktrainer.com.R;
import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.Utility.Utils;
import stocktrainer.com.constant.Constant;
import stocktrainer.com.settings.server.ChangePasswordPOSTRequest;
import stocktrainer.com.settings.server.UpdateProfilePOSTRequest;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mContext;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chnage_password);
        mContext=this;

        //set action bar
        setActionBar();

        //set font style
        setFontStyle();


        //initialise component
        initComponent();

    }

    private void initComponent(){
        (findViewById(R.id.btnSubmit)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgvwHeaderBack:
                finish();
                break;

            case R.id.btnSubmit:
                String strOldPassword = ((EditText)findViewById(R.id.edtOldPassword)).getText().toString().trim();
                String strNewPassword = ((EditText)findViewById(R.id.edtNewPassword)).getText().toString().trim();
                String strConfirmNewPassword = ((EditText)findViewById(R.id.edtConfirmNewPassword)).getText().toString().trim();
                if(strOldPassword.trim().length()==0){
                    Toast.makeText(mContext,mContext.getString(R.string.old_password_blank),Toast.LENGTH_LONG).show();
                }else if(strNewPassword.trim().length()==0){
                    Toast.makeText(mContext,mContext.getString(R.string.new_password_blank),Toast.LENGTH_LONG).show();
                }else if(!strConfirmNewPassword.equals(strNewPassword)){
                    Toast.makeText(mContext,mContext.getString(R.string.password_not_match),Toast.LENGTH_LONG).show();
                }else{
                    changePassword(strOldPassword,strNewPassword);
                }

                break;
            default:
                break;
        }
    }

    private void setFontStyle() {
        ((Button) findViewById(R.id.btnSubmit)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((EditText) findViewById(R.id.edtOldPassword)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((EditText) findViewById(R.id.edtNewPassword)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((EditText) findViewById(R.id.edtConfirmNewPassword)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
    }

    private void setActionBar(){
        mToolbar= (Toolbar) findViewById(R.id.toolbarChangePassword);
        setSupportActionBar(mToolbar);

        ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));
        ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setText(mContext.getString(R.string.Change_Password));
        (mToolbar.findViewById(R.id.imgvwHeaderBack)).setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    public void changePassword(String old_password,String new_password) {

        Map<String, Object> params = new HashMap<>();
        params.put("old_pass", old_password);
        params.put("new_pass", new_password);

        new ChangePasswordPOSTRequest(mContext,params,new ChangePasswordPOSTRequest.ChangePasswordPOSTRequestCallback(){

            @Override
            public void OnCompleteCallback(JSONObject obj, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try{
                        if (status.getCode() == 200) {
                            try{
                                if(obj.getString("status").equals("true")){
                                    Toast.makeText(mContext,mContext.getString(R.string.Password_Updated),Toast.LENGTH_LONG).show();
                                    finish();
                                }else{
                                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(mContext,mContext.getString(R.string.Server_not_respond),Toast.LENGTH_LONG).show();
                }
            }
        } );
    }
}
