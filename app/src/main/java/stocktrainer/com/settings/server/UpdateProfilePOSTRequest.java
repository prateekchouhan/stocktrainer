package stocktrainer.com.settings.server;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.Map;

import stocktrainer.com.Utility.PreferenceClass;
import stocktrainer.com.constant.Constant;

public class UpdateProfilePOSTRequest {

	public static final String  BASE_URL = Constant.AppConstant.BASE_URL;
    public static final String  UPDATE_PROFILE_URL = BASE_URL + Constant.APIMethods.UPDATE_PROFILE;
    private UpdateProfilePOSTRequestCallback callback;

    public interface UpdateProfilePOSTRequestCallback {
        public void OnCompleteCallback(JSONObject obj, AjaxStatus status);
    }

    public UpdateProfilePOSTRequest(Context mContext, Map<String, Object> params , UpdateProfilePOSTRequestCallback caller) {
        AQuery aq = new AQuery(mContext);
        this.callback = caller;

        aq.ajax(UPDATE_PROFILE_URL, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jObject, AjaxStatus status) {
                try {
                    if(callback != null){
                        callback.OnCompleteCallback(jObject,status);
                    }else{
                        callback.OnCompleteCallback(null,null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.method(AQuery.METHOD_POST).header(Constant.AppConstant.AUTHORIZATION, PreferenceClass.getStringPreferences(mContext,Constant.User.AUTHORIZATION_TOKEN)));

    }
}
