package stocktrainer.com.settings;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import stocktrainer.com.R;
import stocktrainer.com.Utility.Utils;

public class SettingOptionActivity extends AppCompatActivity implements View.OnClickListener{
    private Context mContext;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_option);
        mContext=this;

        //set action bar
        setActionBar();

        //set font style
        setFontStyle();

        //initialise component
        initComponent();

    }

    private void initComponent(){

        //hide all option views before visible any one.
        hideAllOptionView();

        String strOption = getIntent().getExtras().getString("SettingOption");

        if(strOption.equals("ForNotification")){
            (findViewById(R.id.layoutNotification)).setVisibility(View.VISIBLE);
            ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setText(mContext.getString(R.string.Notification_Settings));
        }else if(strOption.equals("ForPrivacyPolicy")){
            (findViewById(R.id.layoutPrivacyPolicy)).setVisibility(View.VISIBLE);
            ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setText(mContext.getString(R.string.Privacy_Policy));
        }else if(strOption.equals("ForLanguage")){
            (findViewById(R.id.layoutLanguage)).setVisibility(View.VISIBLE);
            ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setText(mContext.getString(R.string.Select_Language));
        }

    }

    private void hideAllOptionView(){
        (findViewById(R.id.layoutNotification)).setVisibility(View.GONE);
        (findViewById(R.id.layoutPrivacyPolicy)).setVisibility(View.GONE);
        (findViewById(R.id.layoutLanguage)).setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgvwHeaderBack:
                finish();
                break;
            default:
                break;
        }
    }

    private void setFontStyle() {
        ((TextView) findViewById(R.id.txtEnglish)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) findViewById(R.id.txtHindi)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) findViewById(R.id.txtPrivacyPolicyContent)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) findViewById(R.id.txtEmailNotification)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) findViewById(R.id.txtPushNotification)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
        ((TextView) findViewById(R.id.txtEnableAll)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Regular"));
    }

    private void setActionBar(){
        mToolbar= (Toolbar) findViewById(R.id.toolbarSettingOption);
        setSupportActionBar(mToolbar);

        ((TextView)mToolbar.findViewById(R.id.txtTitleToolbar)).setTypeface(Utils.setFontStyle(mContext, "Roboto_Medium"));

        (mToolbar.findViewById(R.id.imgvwHeaderBack)).setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }
}
